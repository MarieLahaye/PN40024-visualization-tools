#!/usr/bin/env python3
# coding: utf-8
import argparse
import os
import pandas as pd
import numpy as np
from functions_gffutils import create_db, get_children, write_gff


def command_line():
    parser = argparse.ArgumentParser(description='''
            Add attributes to GFF3 (Name, Note, GeneID or gene_description).
        '''.replace(12*' ', ''), formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('action', choices=['add-name', 'add-note', 'add-GeneID', 'add-gene-description'],
        help='''
            * add-name: adding 'Name' attribute based on 'ID' attribute value
            * add-note: adding 'Note' attribute for tRNA and ncRNA, it's value will be the feature type.
            * add-GeneID: adding 'GeneID' attribute based on the gene ID (even for the subfeatures)
            * add-gene-description: adding 'gene_description' attribute. For each gene (or transcript).
                Descriptions must be given in an annot file.
                Note: This option requires in input an annot file, given with the argument --annot.
        '''.replace(12*' ', ''))
    parser.add_argument('-i', '--input', help='GFF3 file', required=True)
    parser.add_argument('-o', '--output', help='GFF3 file with new attributes',
        required=True)
    parser.add_argument('-a', '--annot', help='''
        Annot file containing a gene (or transcript) description for each gene (or transcript)
        ''')
    parser.add_argument('-d', '--deleteDB', action='store_true',
        help='Delete the database storing the GFF3 file')
    args = parser.parse_args()
    return args


# Fonction permettant d'ajouter l'attribut 'Name' aux lignes du GFF3 en se basant sur la ...
# ... valeur de l'attribut 'ID'
def add_name(element):
    if "ID" in element.attributes:
        element_id = element['ID'][0]
        element['Name'] = element_id
    else:
        element['Name'] = "{0}.{1}".format('.'.join(element['Parent']), element.featuretype)


# Ajout de l'attribut 'Note' si la feature est un tRNA ou un ncRNA
def add_note(element):
    if element.featuretype in ['ncRNA', 'tRNA']:
        element['Note'] = element.featuretype


# Ajout de l'attribut 'GeneID' en se basant sur l'ID du gène auquel la feature est ...
# ... associée
def add_GeneID(element, gene):
    element['GeneID'] = gene.replace('gene:', '')


# Création d'une Series pandas, l'indice en ligne correspond à l'ID du gène et la valeur ...
# ... correspond à une ligne correspondant à toutes les descriptions (s'il y en a plus d'une)
def series_from_annot(annot_file):
    annot = pd.read_csv(annot_file, sep='\t', header=None)
    dict_annot = {}

    # Set des noms de gènes
    genes = set(annot.iloc[:, 0])
    for gene in genes:
        annots = annot[annot[0] == gene][2].values

        # Suppression des NaN dans l'array
        dict_annot[gene] = annots[~pd.isna(annots)].tolist()
    return pd.Series(dict_annot)


# Ajout de l'attribut 'gene_description'
def add_gene_description(element, annotations):
    if element['ID'][0].replace('gene:', '').replace('mRNA', '') in annotations.index:
        element['gene_description'] = ','.join(annotations[element['ID'][0].replace('gene:', '')])
    else:
        element['gene_description'] = 'unknown'


# En fonction de l'option donnée dans la ligne de commande, on appellera la fonction ...
# ... permettant d'ajouter le ou les attributs désirés
def add_attribute(finput, foutput, chosen_action, fannot):
    print("Creating GFF3 database...")
    gff_db = create_db(finput)
    list_gff_lines = []
    if chosen_action == "add-name":
        print("Adding 'Name' attribute...")
        for feature in gff_db.all_features():
            add_name(feature)
            list_gff_lines.append('{}\n'.format(feature))
    elif chosen_action == "add-note":
        print("Adding 'Note' attribute...")
        for feature in gff_db.all_features():
            add_note(feature)
            list_gff_lines.append('{}\n'.format(feature))
    elif chosen_action == "add-GeneID":
        print("Adding 'GeneID' attribute...")
        for gene in gff_db.features_of_type("gene"):
            gene_id = gene['ID'][0]
            for feature in [gene, *get_children(gff_db, gene_id)]:
                add_GeneID(feature, gene_id)
                list_gff_lines.append('{}\n'.format(feature))
    elif chosen_action == "add-gene-description":
        print("Creating pandas Series from {} file...".format(fannot))
        annotations_series = series_from_annot(fannot)
        print("Adding 'gene-description' attribute...")
        for feature in gff_db.all_features():
            if not feature.featuretype in ['CDS', 'exon']:
                add_gene_description(feature, annotations_series)
            list_gff_lines.append('{}\n'.format(feature))

    # Ecriture du fichier GFF3 avec les attributs ajoutés
    write_gff(foutput, list_gff_lines)


if __name__ == '__main__':
    args = command_line()
    add_attribute(args.input, args.output, args.action, args.annot)
    #series_from_annot(args.annot)

    # Suppression de la dase de données 'gff_db' si l'utilisateur le souhaite
    if args.deleteDB:
        os.remove('gff_db')

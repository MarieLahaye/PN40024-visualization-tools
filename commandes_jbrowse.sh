### Installation du JBrowse pour PN40024.v4 ###
## Installation serveur HTTP Apache 2 ##
apt-get install apache2

# le dossier /var/www/html/ est alors créé, il faudra mettre les applications ...
# ... web dedans (ex: JBrowse)

## Installation et configuration du JBrowse ##
cd /home/
git clone https://github.com/GMOD/jbrowse.git

cd /var/www/html/
ln -s /home/jbrowse/

cd /home/jbrowse/
./setup.sh
mkdir data/

cd data/
mkdir PN40024.v4
ln -s /home/scripts/

### Intégration des données ###
## Ajout de l'organisme au sélecteur jbrowse ##
# Mettre dans le fichier de configuration générale ces lignes --> /home/jbrowse/jbrowse.conf
'''
[datasets.PN40024_v4]
url  = ?data=data/PN40024.v4/
name = PN40024.v4
'''

## Séquence de référence (FASTA) - Catégorie "1. Reference Sequence" ##
cd /home/jbrowse/data/PN40024.v4/
ln -s /home/data/PN40024.v4/0._Reference_sequence/ 1._Reference_sequence

# Indexation du fichier FASTA #
cd /home/jbrowse/data/PN40024.v4/1._Reference_sequence/
samtools faidx PN40024_40X_merged_ALT_REF.fasta

# Création de la track #
cd /home/jbrowse/data/PN40024.v4/
/home/scripts/initialization_tracks_jbrowse.py -a 1._Reference_sequence/PN40024_40X_merged_ALT_REF.fasta -c trackList.json

## Annotations ALT et REF (GFF) - Catégorie "2. PN40024.v4.1 annotations ##
cd /home/jbrowse/data/PN40024.v4/
ln -s /home/data/PN40024.v4/1._PN40024.v4.1_annotation/ 2._PN40024.v4.1_annotations

# REF #
cd /home/jbrowse/data/PN40024.v4/2._PN40024.v4.1_annotations/

# Ajout notes tRNA et rRNA (ajouté de 'REF' dans le nom du fichier avant le 1er '.' pour que le script initialization_tracks.py puisse changer le ...
# ... nom de la track en conséquence)
/home/scripts/add_note_gff.py -i PN40024.v4.1.REF.gff3 -o PN40024_REF.v4.1.REF.notes.gff3

# Tri, compression, indexation
gt gff3 -sortlines -tidy -retainids PN40024_REF.v4.1.REF.notes.gff3 > PN40024_REF.v4.1.REF.notes.sorted.gff3
bgzip PN40024_REF.v4.1.REF.notes.sorted.gff3
tabix -p gff PN40024_REF.v4.1.REF.notes.sorted.gff3.gz

# Création de la track
cd /home/jbrowse/data/PN40024.v4/
/home/scripts/initialization_tracks_jbrowse.py -a 2._PN40024.v4.1_annotations/PN40024_REF.v4.1.REF.notes.sorted.gff3.gz -c trackList.json

# ALT #
cd /home/jbrowse/data/PN40024.v4/2._PN40024.v4.1_annotations/

# ajout notes tRNA et rRNA
/home/scripts/add_note_gff.py -i PN40024.v4.1.ALT.gff3 -o PN40024_ALT.v4.1.ALT.notes.gff3

# tri, compression, indexation
gt gff3 -sortlines -tidy -retainids PN40024_ALT.v4.1.ALT.notes.gff3 > PN40024_ALT.v4.1.ALT.notes.sorted.gff3
bgzip PN40024_ALT.v4.1.ALT.notes.sorted.gff3
tabix -p gff PN40024_ALT.v4.1.ALT.notes.sorted.gff3.gz

# creation de la track
cd /home/jbrowse/data/PN40024.v4/
/home/scripts/initialization_tracks_jbrowse.py -a 2._PN40024.v4.1_annotations/PN40024_ALT.v4.1.ALT.notes.sorted.gff3.gz -c trackList.json

## Index des names des features ##
/var/www/html/jbrowse/bin/generate-names.pl --out .

## Annotations des assembalges précédents - Catégorie "3. Previous annotations versions" ##
cd /home/jbrowse/data/PN40024.v4/
ln -s /home/data/PN40024.v4/2._Previous_annotation_versions/ 3._Previous_annotation_versions

# Correction des erreurs de formatage et sort des lignes #
# Plusieurs corrections sur le fichier VCOSTv3.ori.gff3 doivent se faire à la main. On copie tout d'abord le fichier pour pouvoir garder l'original intact
cd /home/jbrowse/data/PN40024.v4/3._Previous_annotation_versions
cp VCOSTv3.gff3 intermediate_files_jbrowse/VCOSTv3.corrected.gff3

# 1. Gène Vitvi15g01341 - ajout d'un ";" avant l'attribut "Note"
# 2. mRNA Vitvi16g01454.t01 - ajout de l'attribut "Parent" --> nécessaire pour la hiérarchie du fichier
# 3. 4 lignes avaient des attributs "Parent" qui se terminaient par ",;", il a fallu supprimer le "," 
# Après ces modifications faites à la main, il faut renommer les ID des gènes dupliqués, ainsi que de ses subfeatures avec le script rename_duplicates.py
# Dans le fichier VCOSTv3.corrected.gff3, il y a aussi des CDS dupliqués. Ce sont des CDS appartenant au même gène mais associés à des transcrits différents, ...
# ... mais ils ont le même ID. Le script rename_duplicates.py permet aussi de renommer les identifiants de ces CDS pour qu'ils soient uniques ...
# .... on ajoute juste le nom du Parent à l'identifiant du CDS
/home/scripts/rename_duplicates.py -i CRIBI_V1.gff3 -o intermediate_files_jbrowse/CRIBI_V1.rename_duplicates.gff3
/home/scripts/rename_duplicates.py -i intermediate_files_jbrowse/VCOSTv3.corrected.gff3 -o intermediate_files_jbrowse/VCOSTv3.rename_duplicates.gff3

# Tri, compression, indexation #
gt gff3 -sortlines -tidy -retainids intermediate_files_jbrowse/CRIBI_V1.rename_duplicates.gff3 > intermediate_files_jbrowse/CRIBI_V1.rename_duplicates.sorted.gff3
bgzip intermediate_files_jbrowse/CRIBI_V1.rename_duplicates.sorted.gff3
tabix -p gff intermediate_files_jbrowse/CRIBI_V1.rename_duplicates.sorted.gff3.gz

gt gff3 -sortlines -tidy -retainids intermediate_files_jbrowse/VCOSTv3.rename_duplicates.gff3 > intermediate_files_jbrowse/VCOSTv3.rename_duplicates.sorted.gff3
bgzip intermediate_files_jbrowse/VCOSTv3.rename_duplicates.sorted.gff3
tabix -p gff intermediate_files_jbrowse/VCOSTv3.rename_duplicates.sorted.gff3.gz

ln -s intermediate_files_jbrowse/CRIBI_V1.rename_duplicates.sorted.gff3.gz
ln -s intermediate_files_jbrowse/CRIBI_V1.rename_duplicates.sorted.gff3.gz.tbi
ln -s intermediate_files_jbrowse/VCOSTv3.rename_duplicates.sorted.gff3.gz
ln -s intermediate_files_jbrowse/VCOSTv3.rename_duplicates.sorted.gff3.gz.tbi

# Création des tracks #
cd /home/jbrowse/data/PN40024.v4/
/home/scripts/initialization_tracks_jbrowse.py -a 3._Previous_annotation_versions/CRIBI_V1.rename_duplicates.sorted.gff3.gz -c trackList.json
/home/scripts/initialization_tracks_jbrowse.py -a 3._Previous_annotation_versions/VCOSTv3.rename_duplicates.sorted.gff3.gz -c trackList.json

## Catégorie "4. Gene catalogue" ##
cd /home/jbrowse/data/PN40024.v4/
ln -s /home/data/PN40024.v4/3._Gene_catalogue/ 4._Gene_catalogue

cd /home/jbrowse/data/PN40024.v4/4._Gene_catalogue/
# Tri, compression et indexation #
gt gff3 -sortlines -tidy -retainids Gene_catalogue.gff > Gene_catalogue.sorted.gff
bgzip Gene_catalogue.sorted.gff
tabix -p gff Gene_catalogue.sorted.gff.gz

# Création de la track #
cd /home/jbrowse/data/PN40024.v4/
/home/scripts/initialization_tracks_jbrowse.py -a 4._Gene_catalogue/Gene_catalogue_test.sorted.gff.gz -c trackList.json

## RNA-Seq - Catégorie "5. RNAseq" ##
cd /home/jbrowse/data/PN40024.v4/
ln -s /home/data/PN40024.v4/4._RNAseq/ 5._RNAseq

# Pas besoin de filtrer les fichiers, ou de les indexer, tout est indiqué dans la partie Apollo et a donc déjà été réalisée

# Création des tracks #
/home/scripts/initialization_tracks_jbrowse.py -a 5._RNAseq/Mixed_samples_-_insertsize_lt_300_-_MQ_gt_40.bam -c trackList.json
/home/scripts/initialization_tracks_jbrowse.py -a 5._RNAseq/PN40024_RNAseq_on_REF.bam -c trackList.json

## Catégorie "6. Orthologs" ##
cd /home/jbrowse/data/PN40024.v4/
ln -s /home/data/PN40024.v4/6._Orthologs/

# Tri, compression et indexation #
cd /home/jbrowse/data/PN40024.v4/6._Orthologs/
/home/scripts/preparegff3.sh -i `ls *.gff3 | tr "\n" ","` -o Gene_evidences/ 2> Gene_evidences/preparegff3.log

# Boucle pour créer els liens symboliques dans le dossier 6._Orthologs
for file in Gene_evidences/*.{gz,gz.tbi}
do
	ln -s $file
done

# Création des tracks
cd /home/jbrowse/data/PN40024.v4/
/home/scripts/initialization_tracks_jbrowse.py -a 6._Orthologs/*.gff3.gz -c trackList.json

## VCF - Catégorie "7. SNP" ##
cd /home/jbrowse/data/PN40024.v4/
ln -s /home/data/PN40024.v4/7._SNP/

# Création de la track #
/home/scripts/initialization_tracks_jbrowse.py -a 7._SNP/PN40024_Illumina_SNP.vcf.gz -c trackList.json

## Catégorie "8. Others" ##
cd /home/jbrowse/data/PN40024.v4/
ln -s /home/data/PN40024.v4/8._Others/

# Fichiers BED #
# Création des tracks
/home/scripts/initialization_tracks_jbrowse.py -a 8._Others/*.bed.gz -c trackList.json

# Régions répétées #
cd /home/jbrowse/data/PN40024.v4/8._Others/

# Tri, compression, indexation
gt gff3 -sortlines -tidy -retainids Repeat_regions.gff > repeats/Repeat_regions.sorted.gff
bgzip repeats/Repeat_regions.sorted.gff
tabix -p gff repeats/Repeat_regions.sorted.gff.gz

ln -s repeats/Repeat_regions.sorted.gff.gz
ln -s repeats/Repeat_regions.sorted.gff.gz.tbi

# Création des tracks
cd /home/jbrowse/data/PN40024.v4/
/home/scripts/initialization_tracks_jbrowse.py -a 8._Others/Repeat_regions.sorted.gff3.gz -c trackList.json
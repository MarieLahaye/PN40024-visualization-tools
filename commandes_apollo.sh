## Configuration VM et installation dépendences ##
# Mise à jour des packages #
sudo apt-get update
sudo apt upgrade
sudo apt autoremove

# Instalaltion sublime text 3 #
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get install sublime-text

# Packages de base #
sudo apt-get install zlib1g zlib1g-dev libexpat1-dev libpng-dev libgd-dev build-essential git software-properties-common make
sudo apt-get install unzip
sudo apt-get install zip

# Java JDK 8 #
sudo apt-get install openjdk-8-jdk
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/  

# NVM #
# Utile pour installe nodejs
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

# Nodejs  (la version 14.16.1 est déjà installée) #
# Environnement d'exécution permettant d’utiliser le JavaScript côté serveur
nvm install 13
nvm use 13 # à lancer à chaque session pour charger la version 13 de node

# Yarn #
npm install -g yarn

# SDKMAN #
curl -s "https://get.sdkman.io" | bash
source "/root/.sdkman/bin/sdkman-init.sh"

# Grails - Groovy - Gradle #
# Grails: framework open source de développpement d'applications web basé sur le langage Groovy
# Gradle: moteur de production permettant de construire des projets en Groovy (entre autre)
sdk install grails 2.5.5
sdk install gradle 2.11
sdk install groovy

# Ant #
# librarie Java et outil en ligne de commande - utilisé majoritairement ppour construire des applications Java
sudo apt install ant

# Tomcat #
# Il ne faut pas installer tomcat sous l'utilisateur root (pb de sécurité), il faut en créer un nouveau qui va lancer tomcat
sudo useradd -m -U -d /opt/tomcat -s /bin/false tomcat

VERSION=9.0.45
wget https://www-eu.apache.org/dist/tomcat/tomcat-9/v${VERSION}/bin/apache-tomcat-${VERSION}.tar.gz -P /tmp
sudo tar -xf /tmp/apache-tomcat-${VERSION}.tar.gz -C /opt/tomcat/ # exctration de l'archive vers /opt/tomcat
sudo ln -s /opt/tomcat/apache-tomcat-${VERSION} /opt/tomcat/latest # lien symbolique vers la dernière version installée
sudo chown -R tomcat: /opt/tomcat # autoriser l'utilisateur créé précédemment à accéder à tomcat
sudo sh -c 'chmod +x /opt/tomcat/latest/bin/*.sh' # rend les scripts du répertoire bin/ exécutables

sudo nano /etc/systemd/system/tomcat.service # création d'un nouveau service
# Copier coller les lignes suivantes:
'''
[Unit]
Description=Tomcat 9 servlet container
After=network.target

[Service]
Type=forking

User=tomcat
Group=tomcat

Environment="JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64"
Environment="JAVA_OPTS=-Djava.security.egd=file:///dev/urandom -Djava.awt.headless=true"

Environment="CATALINA_BASE=/opt/tomcat/latest"
Environment="CATALINA_HOME=/opt/tomcat/latest"
Environment="CATALINA_PID=/opt/tomcat/latest/temp/tomcat.pid"
Environment="CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC"

ExecStart=/opt/tomcat/latest/bin/startup.sh
ExecStop=/opt/tomcat/latest/bin/shutdown.sh

[Install]
WantedBy=multi-user.target
'''


sudo systemctl daemon-reload 
sudo systemctl enable --now tomcat # start tomcat
sudo systemctl status tomcat # check statut de tomcat

sudo ufw allow 8080/tcp # ouverture du port 8080 pour permet l'accès par d'autres machines

# Création de l'utilisateur 'tomcat' qui a les rôles d'admin et manager
sudo nano /opt/tomcat/latest/conf/tomcat-users.xml # création utilisateurs
'''
   <role rolename="admin-gui"/>
   <role rolename="manager-gui"/>
   <user username="tomcat" password="mdp,;:" roles="admin-gui,manager-gui"/>
'''

sudo nano /opt/tomcat/latest/webapps/manager/META-INF/context.xml
sudo nano /opt/tomcat/latest/webapps/host-manager/META-INF/context.xml

# Commandes pour start, stop et restart tomcat
sudo systemctl restart tomcat
sudo systemctl start tomcat
sudo systemctl stop tomcat

# PostgreSQL - version 12 ne fonctionne pas donc installation de la v10 #
# Création du dépôt du fichier de configuration
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Importation de la clé de signatuer du déôt
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Installation de la version 10 de postgresql - le serveur postgresql se lance automatiquement
sudo apt-get -y install postgresql-10

# Clone du dépot git #
git clone https://github.com/GMOD/Apollo.git Apollo

# Test et installation jbrowse et perl #
cd /home/Apollo
./apollo run-local 8085 # port 8080 déjà utilisé par tomcat

# il n'est disponible que via la VM donc d'abord lancer firefox avec la VM puis entrer l'adresse http://localhost:8085/apollo

# Confiration bdd #
adduser apollo # mdp: apollo

# switch sur utilisateur postgres
su - postgres

# ajouter utilisateur à la base de données
createuser -RDIElPS apollo

# création avec l'utilisateur postgres de la bdd qui appartiendra à apollo 
createdb -E UTF-8 -O apollo apollo-production

# modif de la conf d'apollo
cp sample-postgres-apollo-config.groovy apollo-config.groovy
nano apollo-config.groovy # ajouter changer nom utilisateur de la bdd apollo-production par apollo

# Modification de la mémoire de tomcat dans le fichier setenv #
nano /opt/tomcat/apache-tomcat-9.0.45/bin/setenv.sh

# A écrire
export CATALINA_OPTS="-Xms512m -Xmx10g \
        -XX:+CMSClassUnloadingEnabled \
        -XX:+CMSPermGenSweepingEnabled \
        -XX:+UseConcMarkSweepGC"

sudo systemctl restart tomcat # pour prendre en compte les modifs
ps -ef | grep java # vérifier que les modifs ont été faites

# Changer la mémoire dans la config d'apollo
nano apollo-config.groovy

# Décommenter et mettre la mémoire max à 10G 
'''
// Uncomment to change the default memory configurations
grails.project.fork = [
        test   : false,
        // configure settings for the run-app JVM
        run    : [maxMemory: 10000, minMemory: 64, debug: false, maxPerm: 1024, forkReserve: false],
        // configure settings for the run-war JVM
        war    : [maxMemory: 10000, minMemory: 64, debug: false, maxPerm: 1024, forkReserve: false],
        // configure settings for the Console UI JVM
        console: [maxMemory: 10000, minMemory: 64, debug: false, maxPerm: 1024]
]
'''

# Deploy #
./apollo deploy

# copier le .war dans le dossier tomcat
cp /home/Apollo/target/apollo-2.6.5-SNAPSHOT.war /opt/tomcat/apache-tomcat-9.0.45/webapps/apollo.war
sudo systemctl restart tomcat

# Autorisation d'écriture dans le dossier apollo_data par WebApollo
chmod 777 apollo_data/

# on peut maintenant accéder au WebApollo depuis l'adresse:
# http://138.102.159.70:8080/apollo/annotator/index

# installation des packages
apt install genometools
apt install tabix
apt install samtools
apt install python3-pip
pip3 install pandas
pip3 install gffutils
apt install bedtools


## Intégration des données au WebApollo ##
# Ajout du fasta - Catégorie "0. Reference Sequence" #
# merge du fasta REF et ALT
cd /home/data/PN40024.v4/0._Reference_sequence/reference_sequence/
cat PN40024_40X_REF_chloro_mito.chr_renamed.fasta PN40024_40X_ALT_HETERO.chr_renamed.names_formatted.fasta > PN40024_40X_merged_ALT_REF.fasta
cd /home/data/PN40024.v4/0._Reference_sequence/
ln -s reference_sequence/PN40024_40X_merged_ALT_REF.fasta

# préparation de la séquence de référence
cd /home/Apollo/apollo_data
ln -s /home/data/PN40024.v4/0._Reference_sequence
/home/Apollo/bin/prepare-refseqs.pl --fasta /home/Apollo/apollo_data/0._Reference_sequence/PN40024_40X_merged_ALT_REF.fasta --out /home/Apollo/apollo_data/

# à partir de là on peut créer l'organisme directement via l'interface web


# Ajout du gff3 - Catégorie "1. PN40024.v4.1 annotation" #
#cd /home/data/PN40024.v4/1._PN40024.v4.1_annotation/
# il faut d'abord formatter le gff pour corriger les erreurs qu'il reste
# ajout d'un ";" entre le Name et l'Alias quand il en manque un
#for gene in `grep "Name=.............Alias=" PN40024_40X_REF_AnnoV2_V4nomenclature_Final.ori.gff3 | sed "s/.*ID=//" | sed "s/;.*//"`
#do 
#sed -i "s/Name=${gene}Alias/Name=${gene};Alias/" PN40024_40X_REF_AnnoV2_V4nomenclature_Final.ori.gff3
#done

# certains Name comprennent des "MERGED", remplacer ces Name par le Parent
#/home/scripts/formatting.py -i PN40024_40X_REF_AnnoV2_V4nomenclature_Final.ori.gff3 -o PN40024_40X_REF_AnnoV2_V4nomenclature_Final.gff3
#sed -i -E $'s/\t\t\t\t\t\t\t\t//g' PN40024_40X_REF_AnnoV2_V4nomenclature_Final.gff3

# création de la track
cd /home/Apollo/apollo_data/
ln -s /home/data/PN40024.v4/1._PN40024.v4.1_annotation/

# avec Canvas
# tri, compression, indexation
# retainids pour conserver les ids donnés dans le fichier d'input
#gt gff3 -sortlines -tidy -retainids PN40024_40X_REF_AnnoV2_V4nomenclature_Final.gff3 > PN40024_40X_REF_AnnoV2_V4nomenclature_Final.sorted.gff3
#bgzip PN40024_40X_REF_AnnoV2_V4nomenclature_Final.sorted.gff3
#tabix -p gff PN40024_40X_REF_AnnoV2_V4nomenclature_Final.sorted.gff3.gz
#ln -s /home/data/PN40024.v4/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.sorted.gff3.gz
#ln -s /home/data/PN40024.v4/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.sorted.gff3.gz.tbi
#/home/scripts/initialization_tracks_apollo.py -c trackList.json -a PN40024_40X_REF_AnnoV2_V4nomenclature_Final.sorted.gff3.gz

# avec HTML
/home/Apollo/bin/flatfile-to-json.pl --gff /home/Apollo/apollo_data/1._PN40024.v4.1_annotation/PN40024.v4.1.REF.gff3 --type mRNA --tracklabel "REF" --config '{ "category": "1. PN40024.v4.1 annotation" }' --out /home/Apollo/apollo_data/
/home/Apollo/bin/flatfile-to-json.pl --gff /home/Apollo/apollo_data/1._PN40024.v4.1_annotation/PN40024.v4.1.ALT.gff3 --type mRNA --tracklabel "ALT" --config '{ "category": "1. PN40024.v4.1 annotation" }' --out /home/Apollo/apollo_data/

# indexation des gènes #
bin/generate-names.pl --verbose --out /home/Apollo/apollo_data/


# Ajout de la mise à jour des annotations #
cd /home/Apollo/apollo_data/
ln -s /home/data/PN40024.v4/1.bis_PN40024.v4.2_annotation/
/home/Apollo/bin/flatfile-to-json.pl --gff /home/Apollo/apollo_data/1.bis_PN40024.v4.2_annotation/PN40024.v4_REF.v2.september_21_2021.gff3 --type mRNA --tracklabel "PN40024.v4.2 september 21 2021 REF" --config '{ "category": "1.bis PN40024.v4.2 annotation" }' --out /home/Apollo/apollo_data/

# indexation des gènes #
bin/generate-names.pl --verbose --out /home/Apollo/apollo_data/


# Annotations précédentes -Catégorie "2. Previous annotation versions" #
# CRIBIv1
# doing the v1 liftoff
liftoff -g /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/cribi_V1_on_assembly_12X_V2.gff3 -o v1_to_REF_liftoff.gff3 /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/PN40024_40X_REF_chloro_mito.chr_renamed.fasta /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/pinot_noir_VCost_genome.fasta
# v1 liftoff ALT in the right folder for . output
liftoff -g /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/cribi_V1_on_assembly_12X_V2.gff3 -o v1_to_ALT_HET_liftoff.gff3 /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/PN40024_40X_ALT_HETERO.chr_renamed.names_formatted.fasta /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/pinot_noir_VCost_genome.fasta

# VCOSTv3
# doing the v3 liftoff (there is already a liftover that has been done; but doing this for potential comparison)
liftoff -g /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/VCost.v3_27.gff3 -o v3_to_REF_liftoff.gff3 /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/PN40024_40X_REF_chloro_mito.chr_renamed.fasta /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/pinot_noir_VCost_genome.fasta
# v3 liftoff ALT in the right folder for . output
liftoff -g /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/VCost.v3_27.gff3 -o v3_to_ALT_HET_liftoff.gff3 /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/PN40024_40X_ALT_HETERO.chr_renamed.names_formatted.fasta /home/data/PN40024.v4/2._Previous_annotation_versions/input_files/pinot_noir_VCost_genome.fasta

# Liens symboliques fichiers finaux
cd /home/data/PN40024.v4/2._Previous_annotation_versions
ln -s liftoffs/merged_liftoffs/merged_V3_REF_ALT_HET_liftoff_NOTES_V2.gff3 VCOSTv3.gff3
ln -s liftoffs/merged_liftoffs/v1_to_REF_ALT_HET_liftoff_concat.gff3 CRIBI_V1.gff3

# Création des tracks
cd /home/Apollo/apollo_data/
ln -s /home/data/PN40024.v4/2._Previous_annotation_versions/
/home/Apollo/bin/flatfile-to-json.pl --gff /home/Apollo/apollo_data/2._Previous_annotation_versions/VCOSTv3.gff3 --type mRNA --tracklabel "VCOSTv3" --config '{ "category": "2. Previous annotation versions" }' --out /home/Apollo/apollo_data/
/home/Apollo/bin/flatfile-to-json.pl --gff /home/Apollo/apollo_data/2._Previous_annotation_versions/CRIBI_V1.gff3 --type mRNA --tracklabel "CRIBI V1" --config '{ "category": "2. Previous annotation versions" }' --out /home/Apollo/apollo_data/

# indexation des gènes #
bin/generate-names.pl --verbose --out /home/Apollo/apollo_data/


# Gene catalogue - Catégorie "3. Gene catalogue"#
cd /home/Apollo/apollo_data/
ln -s /home/data/PN40024.v4/3._Gene_catalogue/
/home/Apollo/bin/flatfile-to-json.pl --gff /home/data/PN40024.v4/3._Gene_catalogue/Gene_catalogue.gff --type mRNA --tracklabel "Gene catalogue" --config '{ "category": "3. Gene catalogue" }' --out /home/Apollo/apollo_data/

# indexation des gènes #
bin/generate-names.pl --verbose --out /home/Apollo/apollo_data/


# RNAseq - Catégorie "4. RNAseq" #
cd /home/Apollo/apollo_data/
ln -s /home/data/PN40024.v4/4._RNAseq/

# Mixed samples
cd /home/data/PN40024.v4/4._RNAseq/mixed_samples/

# filtre des BAM par insert size et mapping quality
bamtools filter -insertSize "<=300" -in merged_bams.depth_max_250.12_samples.REF_and_ALT_Hetero.bam -out merged_bams.depth_max_250.12_samples.REF_and_ALT_Hetero.insertsize_300bp.bam
samtools view -bq 40 merged_bams.depth_max_250.12_samples.REF_and_ALT_Hetero.insertsize_300bp.bam > merged_bams.depth_max_250.12_samples.REF_and_ALT_Hetero.insertsize_300bp_mq_40.bam

# Renommage des fichiers
mv merged_bams.depth_max_250.12_samples.REF_and_ALT_Hetero.bam Mixed_samples.bam
mv merged_bams.depth_max_250.12_samples.REF_and_ALT_Hetero.bam.bai Mixed_samples.bam.bai
mv merged_bams.depth_max_250.12_samples.multiple_alignements.REF_and_ALT_Hetero.bam Mixed_samples_with_multimapping.bam
mv merged_bams.depth_max_250.12_samples.multiple_alignements.REF_and_ALT_Hetero.bam.bai Mixed_samples_with_multimapping.bam.bai
mv merged_bams.depth_max_250.12_samples.REF_and_ALT_Hetero.insertsize_300bp_mq_40.bam Mixed_samples_-_insertsize_lt_300_-_MQ_gt_40.bam
mv merged_bams.depth_max_250.12_samples.REF_and_ALT_Hetero.insertsize_300bp_mq_40.bam.bai Mixed_samples_-_insertsize_lt_300_-_MQ_gt_40.bam.bai

cd /home/data/PN40024.v4/4._RNAseq/
ln -s mixed_samples/*

/home/scripts/initialization_tracks_apollo.py -a 4._RNAseq/Mixed_samples.bam -c trackList.json
/home/scripts/initialization_tracks_apollo.py -a 4._RNAseq/Mixed_samples_with_multimapping.bam -c trackList.json
/home/scripts/initialization_tracks_apollo.py -a 4._RNAseq/Mixed_samples_-_insertsize_lt_300_-_MQ_gt_40.bam -c trackList.json

# Mica
cd /home/data/PN40024.v4/4._RNAseq/
ln -s Mica_samples/merged_bam_Mica_REF.250_depth.sort.bam PN40024_RNAseq_on_REF.bam
ln -s Mica_samples/merged_bam_Mica_REF.250_depth.sort.bam.bai PN40024_RNAseq_on_REF.bam.bai

cd /home/Apollo/apollo_data/
/home/Apollo/bin/add-bam-track.pl --bam_url 4._RNAseq/PN40024_RNAseq_on_REF.bam --label "PN40024 RNAseq on REF" --config '{ "category": "4. RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json

# GTs 
cd /home/data/PN40024.v4/4._RNAseq/
ln -s GT_combined/GTs_combined_capped_sort.bam Mixed_runs_GTs.bam
ln -s GT_combined/GTs_combined_capped_sort.bam.bai Mixed_runs_GTs.bam.bai

cd /home/Apollo/apollo_data/
/home/Apollo/bin/add-bam-track.pl --bam_url 4._RNAseq/Mixed_runs_GTs.bam --label "Mixed runs GTs" --config '{ "category": "4. RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json


# RNAseq - Catégorie "4.bis_RNAseq_GFF3" #
cd /home/data/PN40024.v4/4.bis_RNAseq_GFF3
ln -s corvina_transcriptoma/de_novo_full_Corvina_merge.gff3 Corvina_de_novo.gff3
ln -s CS_IsoSeq/CS_IsoSeq.CISIs.95_CI.gff3 CS_IsoSeq_CISIs_95_CI.gff3
ln -s merlot_mybs/de_novo_MYBs_merge.gff3 MYBs.gff3

cd /home/Apollo/apollo_data/
/home/Apollo/bin/flatfile-to-json.pl --gff 4.bis_RNAseq_GFF3/Corvina_de_novo.gff3 --type mRNA --trackLabel "Corvina de novo" --config '{ "category": "4.bis RNAseq GFF3" }' --out /home/Apollo/apollo_data/
/home/Apollo/bin/flatfile-to-json.pl --gff 4.bis_RNAseq_GFF3/CS_IsoSeq_CISIs_95_CI.gff3 --type mRNA --trackLabel "CS IsoSeq CISIs 95 CI" --config '{ "category": "4.bis RNAseq GFF3" }' --out /home/Apollo/apollo_data/
/home/Apollo/bin/flatfile-to-json.pl --gff 4.bis_RNAseq_GFF3/MYBs.gff3 --type mRNA --trackLabel "MYBs" --config '{ "category": "4.bis RNAseq GFF3" }' --out /home/Apollo/apollo_data/

# RNAseq - Catégorie "5._Separate_RNAseq"
cd /home/Apollo/apollo_data/
ln -s /home/data/PN40024.v4/5._Separate_RNAseq/

/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/RCDN1_S1_Vitis_sylvestris_Green_berries.sorted.bam --label "RCDN1_S1 Vitis sylvestris Green berries" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/RCDN5_S2_Vitis_sylvestris_Mid-ripening_berries.sorted.bam --label "RCDN5_S2 Vitis sylvestris Mid-ripening berries" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/SRR1502882_Carignan_Leaves_Erysiphe_necator_infection.sorted.bam --label "SRR1502882 Carignan Leaves Erysiphe necator" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/SRR2015361_Riesling_Shoot_limestone_treated_soil.sorted.bam --label "SRR2015361 Riesling Shoot Limestone treated soil" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/SRR4447140_Jingxiangyu_Leaves_BBCH16.sorted.bam --label "SRR4447140 Jingxiangyu Leaves BBCH16" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/SRR522298_Corvina_Bud-Inflorescence-Tendril-Leaf-Berry-Seed-Rachis-Stem-Root.sorted.bam --label "SRR522298 Corvina Bud-Inflorescence-Tendril-Leaf-Berry-Seed-Rachis-Stem-Root" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/SRR5435950_Pinot_noir_Leaves_Plasmopara_viticola_infection.sorted.bam --label "SRR5435950 Pinot noir Leaves Plasmopara viticola" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/SRR6195043_Vlaska_Leaf_petioles.sorted.bam --label "SRR6195043 Vlaska Leaf petioles" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/SRR6365708_Bangalore_blue_Leaves.sorted.bam --label "SRR6365708 Bangalore blue Leaves" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/SRR7192360_Cabernet_Franc_Leaves_GRBaV_infected.sorted.bam --label "SRR7192360 Cabernet Franc Leaves GRBaV" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/SRR7451534_Victoria_Leaves_White_rot_infection.sorted.bam --label "SRR7451534 Victoria Leaves White rot" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json
/home/Apollo/bin/add-bam-track.pl --bam_url 5._Separate_RNAseq/SRR7768546_Rosario_Bianco_Buds.sorted.bam --label "SRR7768546 Rosario Bianco Buds" --config '{ "category": "5. Separate RNAseq", "type": "Alignments2" }' --out /home/Apollo/apollo_data/trackList.json --in /home/Apollo/apollo_data/trackList.json


# Orthologues - Catégorie "6._Orthologs" #
cd /home/data/PN40024.v4/6._Orthologs
ln -s Gene_evidences/ARA_rn.gff3 Arabidopsis.rn.gff3
ln -s Gene_evidences/ORTHODB_rn.gff3 ORTHODB.rn.gff3
ln -s Gene_evidences/SWISSPROT_rn.gff3 SWISSPROT.rn.gff3
ln -s Gene_evidences/Vitales_rn.gff3 Vitales.rn.gff3

cd /home/Apollo/apollo_data/
ln -s /home/data/PN40024.v4/6._Orthologs/
/home/Apollo/bin/flatfile-to-json.pl --gff 6._Orthologs/Arabidopsis.rn.gff3 --type mRNA --trackLabel "Arabidopsis" --config '{ "category": "6. Orthologs" }' --out /home/Apollo/apollo_data/
/home/Apollo/bin/flatfile-to-json.pl --gff 6._Orthologs/ORTHODB.rn.gff3 --type mRNA --trackLabel "ORTHODB" --config '{ "category": "6. Orthologs" }' --out /home/Apollo/apollo_data/
/home/Apollo/bin/flatfile-to-json.pl --gff 6._Orthologs/SWISSPROT.rn.gff3 --type mRNA --trackLabel "SWISSPROT" --config '{ "category": "6. Orthologs" }' --out /home/Apollo/apollo_data/
/home/Apollo/bin/flatfile-to-json.pl --gff 6._Orthologs/Vitales.rn.gff3 --type mRNA --trackLabel "Vitales" --config '{ "category": "6. Orthologs" }' --out /home/Apollo/apollo_data/


# Variants - Catégorie "7. SNPs" #
cd /home/data/PN40024.v4/7._SNP/
bgzip PN40024_76_114nt_PN40024_40X_REF_ALT_HETERO_chloro_mito.chr_renamed_sorted_dupl_FILTER_AD_PASS.vcf
tabix -p PN40024_76_114nt_PN40024_40X_REF_ALT_HETERO_chloro_mito.chr_renamed_sorted_dupl_FILTER_AD_PASS.vcf.gz

ln -s PN40024_76_114nt_PN40024_40X_REF_ALT_HETERO_chloro_mito.chr_renamed_sorted_dupl_FILTER_AD_PASS.vcf.gz PN40024_Illumina_SNP.vcf.gz
ln -s PN40024_76_114nt_PN40024_40X_REF_ALT_HETERO_chloro_mito.chr_renamed_sorted_dupl_FILTER_AD_PASS.vcf.gz.tbi PN40024_Illumina_SNP.vcf.gz.tbi

cd /home/Apollo/apollo_data
ln -s /home/data/PN40024.v4/7._SNP/
/home/scripts/initialization_tracks_apollo.py -a 7._SNP/PN40024_Illumina_SNP.vcf.gz -c trackList.json


# Autres - Catégorie "8. Others" #
cd /home/Apollo/apollo_data
ln -s /home/data/PN40024.v4/8._Others/
# Régions hétérozygotes #
# REF
cd /home/data/PN40024.v4/8._Others/heterozygous_regions

# tri, compression, indexation
sortBed -i Coord_hetero_REF.chr_renamed.bed > Coord_hetero_REF.chr_renamed.sorted.bed
bgzip Coord_hetero_REF.chr_renamed.sorted.bed
tabix -p bed Coord_hetero_REF.chr_renamed.sorted.bed.gz

cd /home/data/PN40024.v4/8._Others/heterozygous_regions
ln -s heterozygous_regions/Coord_hetero_REF.chr_renamed.sorted.bed.gz Heterozygous_region.bed.gz
ln -s heterozygous_regions/Coord_hetero_REF.chr_renamed.sorted.bed.gz.tbi Heterozygous_region.bed.gz.tbi

# création de la track
cd /home/Apollo/apollo_data
/home/scripts/initialization_tracks_apollo.py -a 8._Others/Heterozygous_region.bed.gz -c trackList.json

# ALT
# pas mettre les régions hétérozygotes ALT car on a gardé uniquement les régions hétérozygotes

# Scaffolds
# tri, compression, indexation
cd /home/data/PN40024.v4/8._Others/scaffolds/
sortBed -i PN40024_40X_REF_Scaffolds.chr_renamed.bed > PN40024_40X_REF_Scaffolds.chr_renamed.sorted.bed
bgzip PN40024_40X_REF_Scaffolds.chr_renamed.sorted.bed
tabix -p bed PN40024_40X_REF_Scaffolds.chr_renamed.sorted.bed.gz

cd /home/data/PN40024.v4/8._Others/
ln -s scaffolds/PN40024_40X_REF_ALT_Hetero_Scaffolds.chr_renamed.sorted.bed.gz Scaffolds.bed.gz
ln -s scaffolds/PN40024_40X_REF_ALT_Hetero_Scaffolds.chr_renamed.sorted.bed.gz.tbi Scaffolds.bed.gz.tbi

# création de la track
cd /home/Apollo/apollo_data
/home/scripts/initialization_tracks_apollo.py -a 8._Others/Scaffolds.bed.gz -c trackList.json

# Missing sequences (Ngaps)
cd /home/data/PN40024.v4/8._Others/Ngaps/

# tri, compression, indexation
sortBed -i PN40024_40X_REF_chloro_mito.Ngaps.bed > PN40024_40X_REF_chloro_mito.Ngaps.sorted.bed
bgzip PN40024_40X_REF_chloro_mito.Ngaps.sorted.bed
tabix -p bed PN40024_40X_REF_chloro_mito.Ngaps.sorted.bed.gz

cd /home/data/PN40024.v4/8._Others/
ln -s Ngaps/PN40024_40X.REF.ALT_HETERO.Ngaps.sorted.bed.gz Ngaps.bed.gz
ln -s Ngaps/PN40024_40X.REF.ALT_HETERO.Ngaps.sorted.bed.gz.tbi Ngaps.bed.gz.tbi

# création de la track des Ngaps
cd /home/Apollo/apollo_data/
/home/scripts/initialization_tracks_apollo.py -a 8._Others/Ngaps.bed.gz -c trackList.json

# Régions répétées
cd /home/data/PN40024.v4/8._Others/
ln -s repeats/TEs_from_v0_to_v4_REF.gff Repeat_regions.gff

# création de la track
cd /home/Apollo/apollo_data/
/home/Apollo/bin/flatfile-to-json.pl --gff /home/Apollo/apollo_data/Repeat_regions.gff --type DHH,DTA,DTC,DTH,DTM,DTT,DTX,DUU,DXX,RIL,RLC,RLG,RLR,RLU,RLX,RXX,XXX --tracklabel "Repeat regions" --config '{ "category": "8. Others" }' --out /home/Apollo/apollo_data/

# backup de la base de données Apollo
su - postgres
pg_dump apollo-production > apollo-production.sql
exit
mv /var/lib/postgresql/apollo-production.sql /home/data/PN40024.v4/BACKUP_CHADO_DATABASE/
# pour restaurer cette base de données 
# psql apollo-production <  apollo-production.sql

#!/bin/bash

# Fonction to display the script help
function usage {
	cat <<-__EOF__
		Usage:
			./preparegff3.sh -i input_files -o outdir [-h]

		Description:
			Sort, compress and index GFF3 files

		Arguments:
			-i, --input List of GFF3 files (required)
				Files must be separated by commas
				You can use the following command: ls path/to/*.gff3 | tr "\n" "," 
			-o, --outdir Output directory  (required)
			-h, --help

		Exemple: ./preparegff3.sh -i \`ls *.gff3 | tr "\n" ","\` -o path/to/outdir
		__EOF__
}

# Eval command line arguments given in input
ARGS=$(getopt -o "i:o:h" --long "input:,outdir:,help" -- $@ 2> /dev/null)

# Check if the return code of the previous command is not equal to 0 (command ...
# ... didn't work)
# >&2 send the message to standard error (stderr) instead of standard out (stdout)
if [ $? -ne 0 ]; then
	echo "Error in the argument list. Use -h or --help to display the help." >&2
	exit 1
fi

eval set -- ${ARGS}
while true
do
	case $1 in
		-i|--input)
			INPUT_FILES=$2
			shift 2
			;;
		-o|--outdir)
			OUTPUT_DIRECTORY=$2
			shift 2
			;;
		-h|--help)
			usage
			exit 0
			;;
		--) shift
			break
			;;
		*)	echo "Option $1 is not recognized. Use -h or --help to display the help." && \
			exit 1
			;;
	esac
done


# Check if one required parameter is missing in the given command line
if [[ ${INPUT_FILES} == "" ]] || [[ ${OUTPUT_DIRECTORY} == "" ]]; then
	echo "Options --input and --outdir are required. Use -h or --help to display the help" >&2
	exit 1
fi

# Check if directory exists
if ! [ -d "${OUTPUT_DIRECTORY}" ]; then
    echo "${OUTPUT_DIRECTORY} doesn't exist. Create it or enter the right path and file name" >&2
    exit 1
fi

for i in $(echo ${INPUT_FILES} | tr "," "\n")
do
	# Check if file exist - if not, display message
	if [ -f $i ]; then
		file=${i##*/}
		file_name=${file%%.*}

		echo "Preparing $i ..."
		# Sort, compress and index
		gt gff3 -sortlines -tidy -retainids $i > ${OUTPUT_DIRECTORY}/${file_name}.sorted.gff3
		bgzip ${OUTPUT_DIRECTORY}/${file_name}.sorted.gff3
		tabix -p gff ${OUTPUT_DIRECTORY}/${file_name}.sorted.gff3.gz
	else
		echo "File doesn't exist. Create it or enter the right path and file name" >&2
	fi
done

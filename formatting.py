#!/usr/bin/python3.8
import argparse
import pandas as pd
import numpy as np


def command_line():
    parser = argparse.ArgumentParser(description='Replace Name annotation by Parent if "MODEL" in it')
    parser.add_argument('-i', '--input', required=True, help='GFF input file')
    parser.add_argument('-o', '--output', required=True, help='GFF output file')
    args = parser.parse_args()
    return args.input, args.output


def format_gff(finput, foutput):
    input_df = pd.read_csv(finput, header=None, names=range(9), sep='\t')

    # conversion des float en int - start et end
    input_df[3] = input_df[3].astype('Int64')
    input_df[4] = input_df[4].astype('Int64')

    # récupère les index des lignes contenant la chaine de caractères 'MERGED'
    merged = input_df.index[input_df[8].str.contains('MERGED') == True].to_list()
    for m in merged:
        if not "gene" in input_df.iloc[m, 2]:
            Parent = ""
            # récupération de l'ID du parent
            Parent = [ipt[7:] for ipt in input_df.iloc[m, 8].split(';') if 'Parent' in ipt][0]

            # liste qui comprend toutes les annotations d'un feature - on ne garde pas les éléments qui ...
            # ... n'ont pas d'"=" car cela signifie que c'est une erreur 
            annotations = [ ipt for ipt in input_df.iloc[m, 8].split(';') if '=' in ipt]
            for j in range(len(annotations)):
                if "MERGED" in annotations[j]:
                    annotations[j] = "Name={}".format(Parent)
        else:
            ID = [ipt[3:] for ipt in input_df.iloc[m, 8].split(';') if 'ID' in ipt][0]
            annotations = [ipt for ipt in input_df.iloc[m, 8].split(';') if '=' in ipt]
            for j in range(len(annotations)):
                if "MERGED" in annotations[j]:
                    annotations[j] = "Name={}".format(ID)
        input_df.iloc[m, 8] = ';'.join(annotations)

    # écriture du fichier de sortie
    input_df.to_csv(foutput, sep='\t', header=False, index=False, na_rep='')


if __name__ == "__main__":
    finput, foutput = command_line()
    format_gff(finput, foutput)


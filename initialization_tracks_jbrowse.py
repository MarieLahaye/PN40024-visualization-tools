#!/usr/bin/env python3
# coding: utf-8
import argparse
import json
import os.path


# Fonction pour définir tous les arguments et options de la ligne de commande
def command_line():
    parser = argparse.ArgumentParser(description='''
        Tracks management of the configuration file trackList.json. Creation,
        update, deletion, addition of metadata.
        ''')
    parser.add_argument(
        '-c', '--conf', required=True,
        help='trackList.json - where the tracks will be added'
        )
    parser.add_argument(
        '-a', '--add', nargs='+',
        help='''
        list of files to add to the jbrowse/list of files for
        which we want to add metadata
        '''
        )
    parser.add_argument(
        '-r', '--remove', nargs='+',
        help='list of tracks label to remove from the jbrowse')
    parser.add_argument(
        '-m', '--metadata',
        help='JSON file with metadata for specific tracks')
    parser.add_argument(
        '--replace', action='store_true',
        help='to replace metadata')
    parser.add_argument(
        '--update', action='store_true',
        help='to update existing tracks by giving the files path')
    args = parser.parse_args()
    return args.conf, args.add, args.remove,\
        args.metadata, args.replace, args.update


# Récupération du nom du fichier bam sans extension, de l'extension ...
# ... du dossier parent et du chemin du fichier à mettre dans l'url du tracks
def path_name(file, json_path):
    name = os.path.basename(file).split('.')[0]
    extension = os.path.basename(file).split('.')[-1]
    if extension == "gz":
        extension = os.path.basename(file).split('.')[-2]
    file_path = file.replace(
        os.path.commonprefix(
            [file, json_path]
        ), "")
    directory = os.path.dirname(file).split('/')[-1]
    # Si le répertoire n'est pas dans le chemin, on regarde le chemin absolu ...
    # ... vers le fichier pour l'ajouter au chemin
    if not directory:
        absolute_path = os.path.abspath(file)
        directory = os.path.dirname(absolute_path).split('/')[-1]
        file_path = "{0}/{1}".format(directory, file_path)
    return name, extension, file_path, directory


# Suppression track en fonction de son label, ou de son chemin --> dans le ...
# ... cas où l'on veut mettre à jour les tracks
def remove_track(name, data, update, file_path=None):
    for track in data['tracks'][:]:
        if update:
            if track['urlTemplate'] == file_path:
                data['tracks'].remove(track)
                break
        else:
            if track['key'] == name:
                data['tracks'].remove(track)


# Fonction pour regrouper les tracks en fonction de la catégorie à laquelle ...
# ... ils sont associés
def tracks_list(data):
    trackList = {
        "1. Reference Sequence": [],
        "2. PN40024.v4.1 annotations": [],
        "3. Previous annotation versions": [],
        "4. Gene catalogue": [],
        "5. RNA-Seq": [],
        "6. Orthologs": [],
        "7. SNP": [],
        "8. Others": []
    }

    for track in data['tracks']:
        for category in trackList.keys():
            if category in track['category']:
                trackList[category].append(track)
    return trackList


# Ajout/Remplacement metadata et création des tracks
def track(data, metadata_file, directory, file_path, name, replace,
          extension, trackList, replace_message, update_message):
    # Création des tracks
    category = directory.replace("_", " ")
    if extension in ['fa', 'fasta']:
        if not trackList['1. Reference Sequence']:
            create_refseq_track(file_path, data)
        else:
            update_message = upd_message(name, category, update_message)
    elif extension in ['bam', 'cram']:
        if name not in [t['label'] for t in trackList['5. RNA-Seq']]:
            create_bam_cram_track(name, category, file_path, data, extension)
        else:
            update_message = upd_message(name, category, update_message)
    elif extension in ["gff3", "gff"]:
        if category == "2. PN40024.v4.1 annotations":
            if "REF" in name:
                key = "PN40024.v4.1 REF"
            elif "ALT" in name:
                key = "PN40024.v4.1 ALT"
            if name not in [t['label'] for t in trackList['2. PN40024.v4.1 annotations']]:
                create_gff_track(file_path, data, category, name, key)
            else:
                update_message = upd_message(name, category, update_message)
        else:
            key = name.replace("_", " ")
            if name not in [t['label'] for t in trackList[category]]:
                create_gff_track(file_path, data, category, name, key)
            else:
                update_message = upd_message(name, category, update_message)
    elif extension == "bed":
        if name not in [t['label'] for t in trackList[category]]:
            create_bed_track(name, category, file_path, data)
        else:
            update_message = upd_message(name, category, update_message)
    elif extension == "vcf":
        if name not in [t['label'] for t in trackList[category]]:
            create_vcf_track(name, category, file_path, data)
        else:
            update_message = upd_message(name, category, update_message)

    # Ajour des metadata
    for d in data['tracks']:
        if name in d['key'] and metadata_file:
            if extension == "bam":
                if "5. RNA-Seq" in d['category']:
                    replace_message = metadata(replace, metadata_file,
                                               d, name, replace_message)
    return replace_message, update_message


def create_base_config():
    track = {
        "formatVersion": 1,
        "refSeqOrder": "name",
        "include": ["functions.conf"],
        "names": {
            "type": "Hash",
            "url": "names/"
        },
        "tracks": []
    }
    json.dump(track, json_file, indent=4)


def create_refseq_track(path, data):
    track = {
        "category": "1. Reference Sequence",
        "chunkSize": 20000,
        "key": "Reference Sequence",
        "label": "DNA",
        "seqType": "dna",
        "storeClass": "JBrowse/Store/SeqFeature/IndexedFasta",
        "type": "SequenceTrack",
        "urlTemplate": path
    }
    data['tracks'].append(track)
    data['refSeqs'] = "{}.fai".format(path)


# Création des tracks pour les fichiers gff
def create_gff_track(path, data, category, name, key):
    track = {
        "category": "{}".format(category.replace("_", " ")),
        "key": "{}".format(key),
        "label": name,
        "labelTranscripts": False,
        "nameAttributes": [
            "ID",
            "Name"
        ],
        "style": {
            "showTooltips": False
        },
        "storeClass": "JBrowse/Store/SeqFeature/GFF3Tabix",
        "topLevelFeatures": "mRNA",
        "type": "CanvasFeatures",
        "urlTemplate": path
    }

    if key in ["Gene catalogue", "Arabidopsis", "ORTHODB", "SWISSPROT", "Vitales"]:
        track['subParts'] = "exon"
    
    if key == "Repeat regions":
        track['topLevelFeatures'] = ['DHH', 'DTA', 'DTC', 'DTH', 'DTM', 'DTT', 'DTX', 'DUU', 'DXX', 'RIL', 'RLC', 'RLG', 'RLR', 'RLU', 'RLX', 'RXX', 'XXX']
    else:
        track['topLevelFeatures'] = "mRNA"
        track['onClick'] = {
            "label": "{subfeatureType}",
            "title": "{name}",
            "action": "defaultDialog"
        }
        track['indexedFeatures'] = [
            "gene",
            "mRNA",
            "exon"
        ]
    data['tracks'].append(track)


# Création des tracks pour les fichiers bam
def create_bam_cram_track(name, category, path, data, extension):
    if extension == "cram":
        store_class = "CRAM"
    elif extension == "bam":
        store_class = "BAM"
    track = {
        "category": category,
        "key": "{}".format(name.replace("_", " ").replace("lt", "<=").replace("gt", ">")),
        "label": name,
        "maxFeatureScreenDensity": 100,
        "storeClass": "JBrowse/Store/SeqFeature/{}".format(store_class),
        "type": "Alignments2",
        "urlTemplate": path
    }
    data['tracks'].append(track)


def create_bed_track(name, category, path, data):
    key = name.replace("_", " ")
    track = {
        "category": category,
        "key": key,
        "label": name,
        "storeClass": "JBrowse/Store/SeqFeature/BEDTabix",
        "type": "CanvasFeatures",
        "urlTemplate": path
    }
    if key == "N gaps":
        track['style'] = {
            "showLabels": False,
            "color": "silver"
        }
        track['onClick'] = {
            "label": "{featureLength}",
            "title": "{name}",
            "action": "defaultDialog"
        }

    data['tracks'].append(track)


def create_vcf_track(name, category, path, data):
    track = {
        "category": category,
        "label": name,
        "key": "{}".format(name.replace("_", " ")),
        "storeClass": "JBrowse/Store/SeqFeature/VCFTabix",
        "urlTemplate": path,
        "type": "JBrowse/View/Track/CanvasVariants"
    }
    data['tracks'].append(track)


# Décide s'il faut ajouter, remplacer les métadonnées ou renvoyer un...
# ... message indiquant qu'elles existent déjà
def metadata(replace, metadata_file, d, name, replace_message):
    if 'metadata' in d.keys() and replace is True:
        add_metadata(metadata_file, d, name)
    elif 'metadata' in d.keys() and replace is False:
        replace_message = repl_message(name, d, replace_message)
    else:
        add_metadata(metadata_file, d, name)
    return replace_message


# Ajout metadata contenues dans le fichier metadata.json
def add_metadata(metadata_file, d, name):
    with open(metadata_file) as metadata:
        meta = json.load(metadata)
        d['metadata'] = {"description": meta[name]}


# Stockage dans une liste, des messages à renvoyer si un track existe déjà...
# ... dans le cas où l'option '--update' n'a pas été indiquée
def upd_message(name, category, message):
    if message:
        message.append(
            "{0} track in {1} category already exists".format(name, category)
        )
    else:
        message = [
            "{0} track in {1} category already exists".format(name, category)
        ]
    return message


# Stockage dans une liste, des messages à renvoyer si les metadata existent...
# ... pour des tracks donnés dans le cas où l'option '--replace' n'a pas...
# ... été indiquée dans la ligne de commande
def repl_message(name, d, message):
    if message:
        message.append(
            "Metadata already exists for {0} track in"
            " {1} category.".format(name, d['category'])
        )
    else:
        message = [
            "Metadata already exists for {0} track in"
            " {1} category.".format(name, d['category'])
        ]
    return message


json_path, create_list, remove_list, metadata_file,\
    replace, update = command_line()
update_message = None
replace_message = None

if __name__ == "__main__":
    # Gestion des exceptions:
    # Except permet de traiter le cas où l'erreur suivante est levée...
    # ... ici il s'agit du cas où le fichier json fournit n'est pas valide...
    # ... par exemple s'il est vide ou qu'il n'existe pas
    try:
        with open(json_path) as json_file:
            data = json.load(json_file)
            pass
    except (json.decoder.JSONDecodeError, FileNotFoundError):
        with open(json_path, 'w') as json_file:
            create_base_config()

    # Load le fichier json puis regarde quelle action réaliser : ...
    # ... suppression de tracks, création de tracks, récupération du...
    # ... nom ou du chemin du fichier, etc
    with open(json_path) as json_file:
        data = json.load(json_file)
        if remove_list:
            for name in remove_list:
                remove_track(name, data, update)

        if create_list:
            for file in create_list:
                name, extension, file_path, directory = path_name(
                    file, json_path
                )
                if update is True:
                    remove_track(name, data, update, file_path)
                trackList = tracks_list(data)
                replace_message, update_message = track(
                    data, metadata_file, directory,
                    file_path, name, replace, extension,
                    trackList, replace_message, update_message
                )

    if replace_message:
        for m in replace_message:
            print(m)
        print("Note: If you want to replace these metadata,"
              " please use the option '--replace'.\n")
    if update_message:
        for m in update_message:
            print(m)
        print("Note: If you want to update these tracks,"
              " please use the option '--update'.\n")

    with open(json_path, 'w') as json_file:
        json.dump(data, json_file, indent=4)

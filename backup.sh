#!/bin/bash

#############################################################
#
# Apollo database backup on the VM and on our Archives server
#
############################################################

# To launch as root on inrae colmar server
user=$( whoami )

if [[ "$user" != "root" ]]
then
  echo 'You are not root user ! exit.'
  exit 1
fi

# Connect to the PN40024 VM to create the apollo backup
# 1. Connect as postgres user
# 2. Keep date
# 3. create the Apollo database backup

cat > script_tmp.sh <<- EOM
#!/bin/bash
day="$(date +'%m_%d_%y')"
pg_dump apollo-production > /var/lib/postgresql/apollo_production_${day}.sql
EOM

chmod 755 script_tmp.sh

scp script_tmp.sh root@138.102.159.70:/home/data/PN40024.v4/BACKUP_CHADO_DATABASE/
ssh root@138.102.159.70 "runuser -u postgres /home/data/PN40024.v4/BACKUP_CHADO_DATABASE/script_tmp.sh"

rm -f script_tmp.sh

# Connect to the PN40024 VM to copy the apollo backup
# 1. Keep date
# 2. copy the backup file to the data folder

cat > script_tmp2.sh <<- EOM
#!/bin/bash
day="$(date +'%m_%d_%y')"
cp /var/lib/postgresql/apollo_production_${day}.sql /home/data/PN40024.v4/BACKUP_CHADO_DATABASE/ || { echo 'There was an error with the backup file copy ! exit.' ; exit 1; }
rm /var/lib/postgresql/apollo_production_${day}.sql
gzip /home/data/PN40024.v4/BACKUP_CHADO_DATABASE/apollo_production_${day}.sql
EOM

chmod 755 script_tmp2.sh

scp script_tmp2.sh root@138.102.159.70:/home/data/PN40024.v4/BACKUP_CHADO_DATABASE/
ssh root@138.102.159.70 "/home/data/PN40024.v4/BACKUP_CHADO_DATABASE/script_tmp2.sh"

rm -f script_tmp2.sh

day=$(date +"%m_%d_%y")
# What to backup.
backup_file="/home/data/PN40024.v4/BACKUP_CHADO_DATABASE/apollo_production_${day}.sql.gz"

# Where to backup to.
dest="/home/crusten/Archives/Apollo_backup/"

# Print start status message.
echo "Backing up $backup_file to $dest" >> /home/crusten/Archives/Apollo_backup/all_logs.log
date >> /home/crusten/Archives/Apollo_backup/all_logs.log

scp root@138.102.159.70:$backup_file $dest

# Print end status message.
echo "Backup finished" >> /home/crusten/Archives/Apollo_backup/all_logs.log

#!/usr/bin/env python3
# coding: utf-8
import argparse
import gffutils
import pandas as pd
import os
import warnings
import sqlite3

# on ignore le warning "RessourceWarning : unclosed file" ...
# ... qui est dû au module "gffutils" qui ne ferme pas ...
# ... le ficher à partir duquel la bdd est créée
warnings.filterwarnings('ignore', category=ResourceWarning)


def command_line():
    parser = argparse.ArgumentParser(description='''
        Rename duplicated genes ID and children 'Parent' ID.
        This script also rename CDS, UTR and exon ID for duplicated and non-duplicated
        genes, by adding 'Parent' ID at the beginning of the subfeature ID. Helps to 
        give unique ID to these subfeatures in case multiple subfeatures have the same
        ID but different 'Parent' ID.
        ''')
    parser.add_argument('-i', '--input', help='GFF input file')
    parser.add_argument('-o', '--output', 
        help='Output file with duplicated genes renamed')
    args = parser.parse_args()
    return args


# Création d'une Séries pandas contenantle nombre d'occurrences de chaque gène ...
# ... on conserve ensuite uniquement les gènes dupliqués
def get_duplicated_genes(finput):
    dict_genes = {}
    with open(finput, 'r') as input_file:
        for line in input_file:
            split_line = line.split('\t')
            if split_line[2] == "gene":
                # Passe la dernière colonne sous la forme d'un dictionnaire et récupération ...
                # ... de l'ID du gène
                gene_id = dict(elt.split('=') for elt in split_line[8].split(';')[:-1])['ID']
                if gene_id not in dict_genes.keys():
                    dict_genes[gene_id] = 1
                else:
                    dict_genes[gene_id] += 1
    # Filtre pour ne conserver que les gènes dupliqués et création d'un Série contenant ...
    # ... le nnombre d'occurrences de ces ID de gènes
    duplicated_genes = {k: v for k, v in dict_genes.items() if v > 1}
    return pd.Series(duplicated_genes)
    


# Création de la feature table
def create_db(finput):
    # Cette ligne permet de changer le comportement par défaut de gffutils ...
    # ... ici on demande à ce qu'une liste ne soit pas retournée s'il n'y a qu'un item dedans
    try:
        db = gffutils.create_db(finput, 'gff_db', merge_strategy='create_unique', keep_order=True)
    except  (sqlite3.OperationalError):
        print("Warning: Table features already exists. Supress it if you want to create a new table.\n")
    db = gffutils.FeatureDB('gff_db')
    return db


# Fonction qui permet de renommer l'ID du gène
def rename_gene_id(gene_renamed, count, list_lines):
    # Il faut stocker le gène dans une nouvelle variable, car sinon impossible de modifier les attributs
    gene_renamed['ID'] = "{0}.{1}".format(gene_renamed['ID'][0], count)
    list_lines.append('{}\n'.format(str(gene_renamed)))


def get_children(gff_db, gene_id, gene):
    gene_children = [child for child in gff_db.children(gene_id)]
    children_list = []
    for child in gene_children:
        if child.seqid == gene.seqid and child.start in range(gene.start, gene.end+1) and \
        child.end in range(gene.start, gene.end+1):
           children_list.append(child)
    return children_list


# Renome les identifiants des subfeatures, ainsi que l'attribut 'Parent'. Si l'ID du gène n'est pas retrouvé ...
# ... dans l'ID du CDS, UTR ou exon qui est en train d'être renommé --> l'ID du Parent sera ajouté au ...
# ... début de l'ID du subfeature
# IMPORTANT : il faut que dans l'ID du mRNA on retrouve le nom du gène, sinon l'ID du mRNA sera différent ...
# ... de l'identifiant retrouvé dans l'attribut 'Parent' de ses subfeatures, ce qui posera un problème ...
# ... d'organisation hiérarchique
def rename_children_parent_id(child_renamed, gene_id, list_lines, count=None):
    if count:
        count_plus_dot = '.{}'.format(count)
    else:
        count_plus_dot = ''

    if gene_id in '.'.join(child_renamed['Parent']):
        child_renamed['Parent'] = [elt.replace(gene_id, '{0}{1}'.format(gene_id, count_plus_dot)) for elt in child_renamed['Parent']]
    else:
        child_renamed['Parent'] = "{0}{1}".format(child_renamed['Parent'][0], count_plus_dot)

    if gene_id in child_renamed['ID'][0]:
        if child_renamed.featuretype in ['CDS', 'exon', 'UTR']:
            child_renamed['ID'] = child_renamed['ID'][0].replace(gene_id, '.'.join(child_renamed['Parent']))
        else:
            child_renamed['ID'] = child_renamed['ID'][0].replace(gene_id, '{0}{1}'.format(gene_id, count_plus_dot))
    else:
        child_renamed['ID'] = "{0}.{1}".format('.'.join(child_renamed['Parent']), child_renamed['ID'][0])

    list_lines.append('{}\n'.format(str(child_renamed)))


def rename_duplicate_genes(finput, foutput):
    list_gff_lines = []
    # Création d'une Série contenant le nombre d'occurrences des gènes dupliqués, et de la bdd
    print("Get genes duplicated occurrence...")
    series_duplicated_genes = get_duplicated_genes(finput)
    print("Table features creation...")
    gff_db = create_db(finput)

    print("Rename genes and subfeatures...")
    for gene in gff_db.features_of_type("gene"):
        gene_id = gene['ID'][0]
        if gene_id in series_duplicated_genes.index:
            count = series_duplicated_genes[gene_id]
            # Renommage du gène
            rename_gene_id(gene, count, list_gff_lines)

            # Récupération des 'enfants' et renommage
            children_list = get_children(gff_db, gene_id, gene)
            for child in children_list:
                rename_children_parent_id(child, gene_id, list_gff_lines, count)
            series_duplicated_genes[gene_id] -= 1
        else:
            list_gff_lines.append('{}\n'.format(str(gene)))
            children_list = get_children(gff_db, gene_id, gene)
            for child in children_list:
                rename_children_parent_id(child, gene_id, list_gff_lines)

    write_gff(foutput, list_gff_lines)


def write_gff(foutput, list_gff_lines):
    output_file = open(foutput, 'w')
    for line in list_gff_lines:
        output_file.write(line)
    output_file.close()


if __name__ == '__main__':
    args = command_line()
    rename_duplicate_genes(args.input, args.output)
    #series_duplicated_genes = get_duplicated_genes(finput)
    
    #gff_db = create_db(args.input)
    #list_gff_lines = []
    #gene = gff_db['VIT_02s0025g04960']
    #gene_id = gene['ID'][0]
    #children_list = get_children(gff_db, gene_id, gene)
    #for child in children_list:
    #    
    #    rename_children_parent_id(child, gene_id, list_gff_lines, 2)
    #    print(child, '\n')

============
Introduction
============

**Access Apollo:** `<http://138.102.159.70:8080/apollo/annotator/index>`_

**Access Jbrowse:** `<http://138.102.159.70/jbrowse/>`_

VM configuration and dependencies installation
----------------------------------------------

Packages
~~~~~~~~
Before starting, you must update the packages :

.. code-block:: bash
	
	sudo apt-get update
	sudo apt upgrade
	sudo apt autoremove

Installation basic packages

.. code-block:: bash

	sudo apt-get install zlib1g zlib1g-dev libexpat1-dev libpng-dev libgd-dev build-essential git software-properties-common make
	sudo apt-get install unzip
	sudo apt-get install zip

Other packages or tools

.. code-block:: bash

	apt install genometools
	apt install tabix
	apt install samtools
	apt install python3-pip
	pip3 install pandas
	apt install bedtools
	pip3 install sphinx

Sublime text 3
~~~~~~~~~~~~~~
On this VM we use sublime text which can be installed with the following commands, but any other text editor for code can be used instead.

.. code-block:: bash

	wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
	sudo apt-get install apt-transport-https
	echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
	sudo apt-get install sublime-text


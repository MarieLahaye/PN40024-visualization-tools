.. apollo-PN40024 documentation master file, created by
   sphinx-quickstart on Tue Jun  1 15:25:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

======================================
Tools for PN40024 genome visualization
======================================

.. toctree::
   :maxdepth: 3

   introduction
   apollo
   jbrowse
   scripts
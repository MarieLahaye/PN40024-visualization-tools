======
Apollo
======

Pre-requisites
--------------

Java JDK
~~~~~~~~

.. code-block:: bash

	sudo apt-get install openjdk-8-jdk

The JAVA_HOME variable must be loaded each time a new session is started, to do it automatically you have to add the following command to the ``~/.bashrc`` file:
	
.. code-block:: bash

	export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/


Nodejs
~~~~~~
**NVM** installation - it is a tool that can install and manage Nodejs versions easier:

.. code-block:: bash

	wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash

Add these lines to export **NVM**:

.. code-block:: bash

	export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
	[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

Run nvm command to install **Nodejs**:

.. code-block:: bash

	nvm install 13

Like **JAVA_HOME** variable, **Nodejs** must be loaded each time a new session is started, so add the following command to the ``~/.bashrc`` file:

.. code-block:: bash

	nvm use 13


Grails - Groovy - Gradle
~~~~~~~~~~~~~~~~~~~~~~~~

**SDKMAN** (like NVM) can manage the versions of packages like grails, gradle and groovy:

.. code-block:: bash

	curl -s "https://get.sdkman.io" | bash
	source "/root/.sdkman/bin/sdkman-init.sh"

**Grails** - **Groovy** - **Gradle** installation:

.. code-block:: bash

	sdk install grails 2.5.5
	sdk install gradle 2.11
	sdk install groovy


Ant
~~~

Java librairy and command line tools - used mostly to run build Java applications:

.. code-block:: bash

	sudo apt install ant


Tomcat
~~~~~~

Because of security risks, tomcat mustn't be run under root use. Instead, we have to create a new user, and a group with ``opt/tomcat/`` as home directory that will run tomcat service:

.. code-block:: bash

	sudo useradd -m -U -d /opt/tomcat -s /bin/false tomcat

Dowloading tomcat zip file and put it to the ``/tmp`` directory:

.. code-block:: bash

	VERSION=9.0.45
	wget https://www-eu.apache.org/dist/tomcat/tomcat-9/v${VERSION}/bin/apache-tomcat-${VERSION}.tar.gz -P /tmp

Archive extraction to tomcat user's home directory, then creation of a symbolic link called ``latest`` that points to the tomcat installation directory:

.. code-block:: bash

	sudo tar -xf /tmp/apache-tomcat-${VERSION}.tar.gz -C /opt/tomcat/
	sudo ln -s /opt/tomcat/apache-tomcat-${VERSION} /opt/tomcat/latest

Change the directory ownership to user and group tomcat created earlier:

.. code-block:: bash

	sudo chown -R tomcat: /opt/tomcat

Shell scripts inside the tomcat's ``bin/`` directory must be executable:

.. code-block:: bash

	sudo sh -c 'chmod +x /opt/tomcat/latest/bin/*.sh'

Instead of using the shell script to start and stop tomcat, we will set it to run as a service. Use nano to edit the ``tomcat.service`` file:

.. code-block:: bash

	sudo nano /etc/systemd/system/tomcat.service

And paste the following configuration:

.. code-block:: bash

	[Unit]
	Description=Tomcat 9 servlet container
	After=network.target

	[Service]
	Type=forking

	User=tomcat
	Group=tomcat

	Environment="JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64"
	Environment="JAVA_OPTS=-Djava.security.egd=file:///dev/urandom -Djava.awt.headless=true"

	Environment="CATALINA_BASE=/opt/tomcat/latest"
	Environment="CATALINA_HOME=/opt/tomcat/latest"
	Environment="CATALINA_PID=/opt/tomcat/latest/temp/tomcat.pid"
	Environment="CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC"

	ExecStart=/opt/tomcat/latest/bin/startup.sh
	ExecStop=/opt/tomcat/latest/bin/shutdown.sh

	[Install]
	WantedBy=multi-user.target

Modify the **JAVA_HOME** path in the ``~/.bashrc``. Open the file with nano:

.. code-block:: bash

	sudo nano ~/.bashrc

And change the variable like the following line. Just change the directory name your java version is different:

.. code-block:: bash

	export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/

When these modifications are done and saved, notify the system that a new unit file exists, then enable and start the tomcat service:

.. code-block:: bash

	sudo systemctl daemon-reload 
	sudo systemctl enable --now tomcat

Tomcat service status can be checked with the command:

.. code-block:: bash

	sudo systemctl status tomcat

To restart, start and stop tomcat:

.. code-block:: bash

	sudo systemctl restart tomcat
	sudo systemctl start tomcat
	sudo systemctl stop tomcat

Tomcat use **port 8080**, we need to open this port to access tomcat from the outside of our local network:

.. code-block:: bash

	sudo ufw allow 8080/tcp

Now that tomcat is installed, we can configure the Tomcat Web Management Interface. Firslty we need to create a user called **tomcat**, for example, with *admin* and *manager* privilegies.

- *admin* allow the user to access the ``host-manager/html`` URL and create, delete and manage virtual hosts
- *manager* allow the use to deploy and undeploy applications

Open the ``tomcat-users.xml`` file:

.. code-block:: bash

	sudo nano /opt/tomcat/latest/conf/tomcat-users.xml

Then uncomment the following lines and create the roles and the user:

.. code-block:: bash

   <role rolename="admin-gui"/>
   <role rolename="manager-gui"/>
   <user username="tomcat" password="mdp,;:" roles="admin-gui,manager-gui"/>

By default the Tomcat web management interface is configure to allow access from the localhost. To allow the admin and manager user to access the interface from outside, open the ``context.xml`` files for the Manager app and the Host Manager app:

.. code-block:: bash

	sudo nano /opt/tomcat/latest/webapps/manager/META-INF/context.xml
	sudo nano /opt/tomcat/latest/webapps/host-manager/META-INF/context.xml

And comment the following lines:

.. code-block:: bash

	<Context antiResourceLocking="false" privileged="true" >
	<!--
	  <Valve className="org.apache.catalina.valves.RemoteAddrValve"
	         allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
	-->
	</Context>

Once the modifications are saved, restart tomcat:

.. code-block:: bash

	sudo systemctl restart tomcat

Now you can see if the installation is successful by accessing to the following address: `<http://138.102.159.70:8080/>`_


PostgreSQL
~~~~~~~~~~

PostgreSQL is the database used by Apollo to store data like sequences, annotations, user informations etc... So the final step before deploying an Apollo instance, is installing postgreSQL.

Create the configuration file repository and import the key signature of the repository:

.. code-block:: bash

	sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
	wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

Then install postgreSQL v10 (the server starts automatically):

.. code-block:: bash

	sudo apt-get -y install postgresql-10

*Note*: version 12 is not compatible with tomcat (or Apollo) so install the version 10 instead.


Apollo installation
-------------------

Clone the git repository:

.. code-block:: bash

	git clone https://github.com/GMOD/Apollo.git Apollo

To test the installation:

.. code-block:: bash

	cd /home/Apollo
	./apollo run-local 8085

Apollo should be available now at the adress: `<http://localhost:8085/apollo>`_

Restarting Apollo
~~~~~~~~~~~~~~~~~~~~~~

Possible error : 

	psql

"psql: la connexion au serveur sur le socket « /var/run/postgresql/.s.PGSQL.5432 » a échoué : No such file or directory
Le serveur est-il actif localement et accepte-t-il les connexions sur ce socket ?"

Solution :

.. code-block:: bash

	# here we can see that the postgres server is down
	pg_lsclusters
	# to see the error 
	sudo pg_ctlcluster 10 main start
	# the solution was :
	sudo chmod 700 -R /var/lib/postgresql/10/main
	# then restart the server
	sudo -i -u postgres
	/usr/lib/postgresql/10/bin/pg_ctl restart -D /var/lib/postgresql/10/main
	#  and restart tomcat
	sudo systemctl restart tomcat

Database configuration
~~~~~~~~~~~~~~~~~~~~~~





Data processing and integration
-------------------------------


Link : http://apollo.colmar.inrae.fr:8080/apollo

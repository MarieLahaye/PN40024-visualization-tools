#!/bin/bash

cd /home/avelt/data2/PN40024-visualization-tools/update_gff3_script
module load genometools/1.5.10
export PATH=/home/avelt/data2/PN40024-visualization-tools/update_gff3_script/conda_env/bin/:/cm/shared/apps/gffread-0.11.5.Linux_x86_64/:$PATH
export PERL5LIB="/home/avelt/data2/PN40024-visualization-tools/update_gff3_script/conda_env/lib/5.26.2/"

export TMPDIR=/home/avelt/data2/PN40024-visualization-tools/update_gff3_script

python update_gff3_apollo.py -f reference_files/VCost.v3_20.cds.longest_transcript.fasta -e reference_files/PN40024_40X_REF_chloro_mito.chr_renamed.fasta \
-c reference_files/PN40024_40X_REF_AnnoV2.gff3.cds.fasta -r reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3 \
-g Annotations_19_01_2022.gff3 -o Annotations.19_january_2022.gff3 \
-w /home/avelt/data2/PN40024-visualization-tools/update_gff3_script/output/ \
-u PN40024.v4_REF.v2.september_21_2021.gff3

#!/usr/bin/env python3

# Author : Amandine Velt - amandine.velt@inrae.fr
# Last update : August 09, 2021

# import packages
import argparse
import warnings
import gffutils
import os
from datetime import date
from datetime import datetime
import sys
from itertools import chain
import numpy as np
from BCBio import GFF
import re
import shutil
import pathlib
from Bio import SeqIO
from gffutils.exceptions import FeatureNotFoundError
import pandas as pd
warnings.simplefilter("ignore", UserWarning)
import pybedtools

# creation of working directories
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', None)

path = pathlib.Path().resolve()
tmp_path= str(path) + "/TMP"
output_path= str(path) + "/output"
databases_path= str(path) + "/databases"

# Check whether the specified path exists or not
isExist = os.path.exists(path)

if not os.path.exists(tmp_path):
  # Create a new directory because it does not exist
  os.makedirs(tmp_path)
  print("The new directory is created!", tmp_path)

if not os.path.exists(output_path):
  # Create a new directory because it does not exist
  os.makedirs(output_path)
  print("The new directory is created!", output_path)

if not os.path.exists(databases_path):
  # Create a new directory because it does not exist
  os.makedirs(databases_path)
  print("The new directory is created!", databases_path)

#--------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------
# Summary

# 1. A "gffutils" (sqlite3) database is created for the two gff3 files :
#       - one database for the PN40024.v4.1 gff3 file (our reference gff3 on Apollo, which will remain the "main" gff3 file)
#       - one database for the Apollo gff3 file (the gff3 file containing all the updated/created genes on Apollo.)
# These two files will allow to create as updated gff3 file.
# See create_database() function

# 2. The next step is to correctly format the "ID" and "Name" attributes of the gff3 coming from Apollo.
#   In this step, I keep only the genes with the value "approved" in the "Status" attribute.
#   TO DO : also process genes on ALT
#   The gene, mRNA, exon and CDS "ID" and "Parent" attributes are created by Apollo and are of type : c78c23a3-67fd-472f-bba5-db8ec9d998f0
#   (because behind the scene there is a database where ID must be unique for each feature type)
#   but, NORMALLY, there is an attribute called "orig_id", with the original ID of the gene or mRNA (orig_id is the ID of the gene drag in
#   the bar to update on Apollo). For some "approved" genes, this "orig_id" is lost during the modification by the user (I don't know why),
#   but usually the "Name" attribute also contains the original ID. Thanks to the "orig_id" or the "Name" attributes, we can rename the gene,
#   mRNA, exon and CDS "ID" and "Parent" attributes.
#   The purpose of this function is to ensure that the ID attribute of each gene, mRNA, exon, CDS finds its original ID. I keep the ID given
#   by Apollo in a new attribute named apollo_id.
#   In addition to finding the original ID of the genes, they must also be formatted.
#   For some "approved" genes, the "orig_id" is like "VIT_12s0028g02670.t01" or the "Name" attribute can be "Vitvi04g01511-00002" etc ...
#   So I format this ID to replace ".t01" or "-00001" by "_t001", and the same for 2, 3, etc ...
#   Also, there is some particular cases, where two different genes have the same ID, or two different mRNAs have the same ID
#   to deal with that, I created "seen_gene" and "seen_mRNA" objects to detect the duplicates and correct the name. For now I add ".2", ".3"
#   and so one but maybe I have to give a "04" ID for these genes. These duplicated IDs come from a split gene, where the two new genes have
#   the same ID (genes involved : Vitvi14g01449, Vitvi18g03067, Vitvi18g02922, , Vitvi07g02254, Vitvi16g01453)
#   This step creates a new, correctly formatted annotation file: Annotations_Apollo.formatted.gff3

#   Some particular cases processed :
#   The genes and mRNAs with an "Approved" status are formatted and kept in the final gff3 file
#   The genes and mRNAs noted "To delete" are stored in a file to be deleted later
#   If the gene is "approved" but the mRNA has the status "to delete", I do not keep the mRNA nor its exons / cds
#   If the "to delete" mRNA is the only mRNA of the gene, I delete the gene too
#   If we have two genes with an identical ID, I add a ".2" at one of them
#   If we have two mRNAs with an identical ID, both with _t001, I increment one of the two so that it becomes t002
#   If the gene and the mRNA do not have the same ID (one case found), I give the gene ID as the mRNA ID.
# See renaming_ID_genes_and_children() function

# 3. A "gffutils" (sqlite3) database is created for the formatted Apollo gff3 file.

# 4. I determine which genes from this new formatted gff3 should be blasted ->
#    - these are the "approved" Apollo genes whose ID is not found in the original PN40024.v4.1 gff3 file
#    - OR the genes from the PN40024.v4.1 gff3 file that have an ID containing "g04 ...."
#   The goal here is to find a reciprocal best hit of these genes in PN12xV2 (VCostV3) to be able to rename them
#   To find the reciprocal best hit, I have to blast all the PN40024.v4 proteins against PN12XV2 proteins (VCostV3)
#   and all the PN12XV2 proteins (VCostV3) against the PN40024.v4 proteins and THEN, find the reciprocal best hit, if it exists.
#   To create the PN40024.v4 fasta file, I take all the Apollo proteins not found in the PN40024.v4.1 proteins fasta file, all the PN40024.v4.#   1 proteins and put them in a common fasta file for the RBH (reciprocal best hit) step.
#   Note : It is mandatory to use all the PN40024.v4.1 proteins for te RBH step :
#   If I only took the new proteins from Apollo, the RBH would be skewed.
#   This step create a fasta file containing all the Apollo sequences and all the REF sequences which are different from those curated and
#   validated in Apollo. This file will be used for  the RBH step against PN12xV2 VCost v3
#   I also create a genes_to_blast_final.txt file containing all the gene IDs to blast (and so to rename). This file will be used later to
#   to find which of these genes have a RBH on PN12XV2 and to rename them.
# See gene_sequences_to_blast() function

# 5. This step do the RBH of all the PN40024.v4.1 + Apollo proteins against all the PN12Xv2 proteins and output a tabulated file with all the
# RBHs found (TMP/reciprocal_best_hits.txt)
# For this step, I use the blast_rbh.py script, available here (https://github.com/peterjc/galaxy_blast/tree/master/tools/blast_rbh)
# Preview of the output file :
# A_id   B_id    A_length        B_length        A_qcovhsp       B_qcovhsp       length  pident  bitscore
# Vitvi01g04001_t001      Vitvi16g01422.t01       230     231     100     99      230     100.000 483
# Vitvi01g00002_t001      Vitvi01g00002.t01       432     443     100     98      432     100.000 894
# Vitvi01g01833_t001      Vitvi01g01833.t01       154     155     100     99      154     100.000 322
# Vitvi01g01834_t001      Vitvi01g01834.t01       115     116     100     99      115     100.000 233
# Vitvi01g00006_t001      Vitvi01g00006.t01       959     929     97      99      928     100.000 1902
# Vitvi01g01835_t001      Vitvi01g01835.t01       225     226     100     99      225     100.000 459
# Vitvi01g01836_t001      Vitvi01g01836.t01       167     168     100     99      167     100.000 349
# Vitvi01g00008_t001      Vitvi01g00008.t01       241     242     100     99      241     100.000 498
# to know :
# - Some genes do not have a RBH, they are genes that we don't find in PN12xv2 and to which we must give an identifier of type "g04...."
# - You can see that there are a lot of genes that have yet the good ID, example : Vitvi01g00002, Vitvi01g01833, Vitvi01g00006 ...
# Usually when the curators create a new gene (which I therefore cannot find in the original gff3) they start from a PN12xv2 gene so generally
# the RBH ID of PN12xv2 is the same as the ID of the "new" gene, not found in the original PN40024.v4 and I haven't to changed the ID
# - If a PN40024.v4 gene has an RBH on PN12Xv2 and the IDs are not the same, then I rename the gene in PN40024.v4 with the ID of its RBH.
# see make_blast_RBH() function

# 6. Now that we have the RBH results file, we need to rename the genes, either with an PN12Xv2 ID if there is an RBH, or with the last ID of
# type "g04..." of the chromosome to which the gene belongs.
# This function juste create a Python dictionnary with the "old ID" of the gene and the corresponding "new ID".
# This dictionary will then be used, when updating gff3, to rename at the same time the genes which must be renamed.
# This function also creates a dictionary with the last ID of type "g04...." for each chromosome.
# to know
# for the list of new genes from Apollo and genes in PN40024.v4 with a "g04...." ID, a gene is renamed with a PN12xV2 ID if there is
# a RBH for this gene and if the two genes are on the same chromosome else, the gene is renamed with a "g04...." ID.
# if there is no RBH, the gene receive the last "g04...." ID of its chromosome
# see rename_genes() function

# 7. In this step, we delete the PN40024.v4.1 genes which have the same ID as the Apollo "approved" genes and we add the updated version of
# this gene
# OR, if the Apollo gene ID is not found in PN40024.v4.1 gff3 file, there is a bedtools intersect (on the same strand)
# if there is an intersection, the gene in PN40024.v4.1 is removed and we add the updated version of this gene
# and finally, we add the new Apollo genes, with no same ID in PN40024.v4.1 file and no intersection.
# At the end, we have a updated gff3, with all the updated/new genes
# see update_REF_gff3_from_Apollo_gff3() function

# 8. In this step, we delete from the updated gff3 file with the new annotations, all the Apollo genes with a status "to delete"
# see delete_TO_DELETE_status_gene_annotations() function

# 9. Thanks to the renaming dictionary generated in step 6, we rename all the genes to be renamed in the updated gff3 file.
# see renaming_genes_from_dict() function

# 10. Because Apollo give a gff3 without the UTRs annotation, I add them with the script "add_utrs_to_gff.py" from the NCBI
# Source : https://ftp.ncbi.nlm.nih.gov/genomes/TOOLS/add_utrs_to_gff/add_utrs_to_gff.py
# I modified this script, see the version in this repository

# 11. The new updated gff3 is validated thanks to the "genometools" tools, with its sort command. This allows to be sure that there is no duplicated IDs, that the gff3 and all the attributes are well formatted, and it allows to separate gene by "###" which is a convention.
#--------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------


# I created this class to perform an ascending AND descending sorting in a same sort command
class reversor:
    def __init__(self, obj):
        self.obj = obj

    def __eq__(self, other):
        return other.obj == self.obj

    def __lt__(self, other):
        return other.obj < self.obj

# we ignore the warning "ResourceWarning: unclosed file" which is due to the "gffutils" module which does not close
# the file from which the database is created
warnings.filterwarnings('ignore', category=ResourceWarning)

# here are all the arguments to provide to the script
def command_line():
  parser = argparse.ArgumentParser(description='''
      Update the original PN40024.v4.1 gff3 file with all updated/new genes from Apollo.
      ''')
  parser.add_argument('-r', '--ref', help='PN40024.v4.1 original GFF3 file.')
  parser.add_argument('-g', '--gff3', help='GFF3 file from Apollo, with all the update.')
  parser.add_argument('-o', '--output', help='GFF3 final output file')
  parser.add_argument('-w', '--workdir', help='Working directory')
  parser.add_argument('-f', '--fasta', help='PN12xv2 cds fasta file to blast PN40024.v4.1 + Apollo CDS (reciprocal best hit)')
  parser.add_argument('-u', '--oldgff3', help='The last gff3 updated file, in order to keep the same Vitvi*g04* ID for the genes renamed with this script before. E.g : PN40024.v4_REF.v2.september_21_2021.gff3')
  parser.add_argument('-e', '--reffasta', help='The PN40024.v4 reference genome file. E.g : reference_files/PN40024_40X_REF_chloro_mito.chr_renamed.fasta')
  parser.add_argument('-c', '--refcds', help='The PN40024.v4 cds sequences. E.g : reference_files/PN40024_40X_REF_AnnoV2.gff3.cds.fasta')
  args = parser.parse_args()
  return args.ref, args.gff3, args.output, args.workdir, args.fasta, args.oldgff3, args.reffasta, args.refcds

# creation of a gffutils database
def create_database(fgff3, db_filename):
  print("------------------")
  print("Creating", db_filename ," database...")
  gffutils.create_db(fgff3, db_filename, sort_attribute_values=True,merge_strategy="create_unique", force=True)
  print("------------------")

# change Name attribute as ID for transcripts
def change_Name_to_ID_PN40024_ref(db_ref,fref):
  filename= str(fref)+ '.Name_formatted.gff3'
  output_file = open(filename, 'w')
  db_ref = gffutils.FeatureDB(db_ref)
  for gene in db_ref.features_of_type('gene'):
    output_file.write("{}\n".format(str(gene)))
    for mRNA in db_ref.children(gene.id, level=1):
      if 'Name' in mRNA.attributes:
        mRNA.attributes['old_name']=mRNA.attributes['Name']
        mRNA.attributes['Name']=mRNA.attributes['ID']
      else:
        print("No name attribute for this mRNA")
        print(mRNA.attributes['ID'])
      output_file.write("{}\n".format(str(mRNA)))
      count_cds=0
      for exon_cds in sorted(db_ref.children(mRNA.attributes['ID'][0], level=1),key=lambda y: (y[3], reversor(y[2]), y[4])):
        output_file.write("{}\n".format(str(exon_cds)))
  output_file.close()

# formating gff3 file from Apollo to a "formatted" gff3 file
def renaming_ID_genes_and_children(db_apollo,foutput):
  print("------------------")
  seen_gene = {}
  seen_mRNA = {}
  change_mRNA_ID="no"
  print("Renaming genes and their children ...")
  db_apollo = gffutils.FeatureDB(db_apollo)
  genes_and_pseudogenes_iterator=chain(db_apollo.features_of_type('gene'), db_apollo.features_of_type('pseudogene'))
  print("---------------------")
  print("Number of 'Approved' genes to process : ")
  count_gene=0
  gene_list=[]
  for gene in db_apollo.features_of_type('gene'):
    try:
      # il peut y avoir des mRNA approved mais le gène n'a pas de status donc on doit garder ce mRNA
      mRNA_approved_but_gene_no_status="NO"
      for mRNA in db_apollo.children(gene.id, level=1):
        if ('status' in mRNA.attributes and 'status' not in gene.attributes):
          if ("Approved" in mRNA.attributes['status']):
            mRNA_approved_but_gene_no_status="YES"
      if ("Approved" in gene.attributes['status'] or mRNA_approved_but_gene_no_status=="YES"):
        count_gene+=1
        gene_list.append(gene.attributes['Name'][0])
    except KeyError:
      # gene has no status attribute, so in process by the curator, pass
      pass
  print(count_gene)
  print(gene_list)
  print("Number of 'Approved' pseudogenes to process : ")
  count_pseudogene=0
  pseudogene_list=[]
  for pseudogene in db_apollo.features_of_type('pseudogene'):
    try:
      # il peut y avoir des mRNA approved mais le gène n'a pas de status donc on doit garder ce mRNA
      mRNA_approved_but_gene_no_status="NO"
      for mRNA in db_apollo.children(pseudogene.id, level=1):
        if ('status' in mRNA.attributes and 'status' not in pseudogene.attributes):
          if ("Approved" in mRNA.attributes['status']):
            mRNA_approved_but_gene_no_status="YES"
      if ("Approved" in pseudogene.attributes['status'] or mRNA_approved_but_gene_no_status=="YES"):
        count_pseudogene+=1
        pseudogene_list.append(pseudogene.attributes['Name'][0])
    except KeyError:
      # gene has no status attribute, so in process by the curator, pass
      pass
  print(count_pseudogene)
  print(pseudogene_list)
  allTheGenes = []
  output_file = open(foutput, 'w')
  output_delete_file = open("to_delete.txt", 'w')
  for gene in genes_and_pseudogenes_iterator:
    mRNA_approved_but_gene_no_status="NO"
    for mRNA in db_apollo.children(gene.id, level=1):
      if ('status' in mRNA.attributes and 'status' not in gene.attributes):
        if ("Approved" in mRNA.attributes['status']):
          mRNA_approved_but_gene_no_status="YES"
    # for now, I don't take into account the genes from ALT_Hetero
    if "_ALT_Hetero" not in gene.chrom:
      try:
        if ('status' in gene.attributes):
          if ("Approved" in gene.attributes['status'] or "To delete" in gene.attributes['status']):
            # some gene Names attribute are Vitvi*g0****, others are Vitvi*g0****.t01, others Vitvi*g0****_t001
            # we have to correct that at the gene level
            # If the gene Name contains Vitvi, The Vitvi**g0**** good format has 13 characters, so we keep only
            # the first 13 characters
            # else if gene ID is not a Vitvi, like WRKY45, we keep it as it is.
            # Later in the script we will try to blast these type of genes and find a Vitvi ID for them
            gene.attributes['apollo_id']=gene.attributes['ID']
            if "Vitvi" in gene.attributes['Name'][0]:
              gene.attributes['Name']=gene.attributes['Name'][0][ 0 : 13 ]
            gene.attributes['ID']=gene.attributes['Name'][0].replace("|", "_").replace(":", "_")
            replacement_gene={".t01": "", "_t001" : "", "_t002" : "", ".t02": "", ".t03": ""}
            for i, j in replacement_gene.items():
              gene.attributes['ID'] = gene.attributes['ID'][0].replace(i, j)
              gene.attributes['Name'] = gene.attributes['Name'][0].replace(i, j)
            allTheGenes.append(gene.attributes['Name'][0])
            # sometimes the gene ID is the same for two different genes,
            # example : Vitvi18g03067
            # so I created seen_gene object containing the gene ID and when there is a duplicate, I add .2 at the end of the ID
            # for example I got Vitvi18g03067 and Vitvi18g03067.2
            # but I don't change the Name, but the ID must to be unique
            if gene.attributes['ID'][0] not in seen_gene:
              seen_gene[gene.attributes['ID'][0]] = 1
            else:
              seen_gene[gene.attributes['ID'][0]] += 1
            if seen_gene[gene.attributes['ID'][0]]>1:
              change_mRNA_ID="yes"
              print("This gene is a split gene :", gene.attributes['ID'])
              print("For now I just add '.2', '.3' etc ... for each part of the split gene.")
              gene.attributes['ID']=gene.attributes['ID'][0] + "." + str(seen_gene[gene.attributes['ID'][0]])
            gene.source="MANUAL_CURATION"
            remove_gene=""
            if ("Approved" in gene.attributes['status']):
              nb_mRNAs=sum(1 for dummy in db_apollo.children(gene.id, level=1))
              for mRNA in db_apollo.children(gene.id, level=1):
                if ('status' in mRNA.attributes):
                  if ("To delete" in mRNA.attributes['status'] and nb_mRNAs == 1):
                    remove_gene="yes"
              if remove_gene != "yes" :
                output_file.write("{}\n".format(str(gene)))
              else:
                output_delete_file.write("{}\n".format(str(gene.attributes['ID'][0])))
            mRNA_transcript_number=1
            nb_mrna=0
            for mRNA in db_apollo.children(gene.id, level=1):
              nb_mrna+=1
              # I keep Apollo ID in a new attribute
              mRNA.attributes['apollo_id']=mRNA.attributes['ID']
              # I keep Apollo Name in a new attribute
              mRNA.attributes['apollo_name']=mRNA.attributes['Name']
              # keep the symbol as attribute to show it on Apollo
              if 'symbol' in gene.attributes:
                if 'Note' in gene.attributes:
                  mRNA.attributes['Note']=gene.attributes['symbol']
                else:
                  mRNA.attributes['description']=gene.attributes['symbol']
              # dictionnary of replacement
              # we have to add a note in the guidelines about the mRNA naming
              replacement={".t01-00001": "_t001",".t01a-00001": "_t001",".t01-00002": "_t001",".t01-00003": "_t001",
              "-MK_TEST" :"_t001","-CK_TEST" :"_t001","a-CK":"_t001",
              "-00001": "_t001", "-00002": "_t002", "-00003": "_t003", "-00004": "_t004", ".t01-CK1": "_t001",
              ".t01-CK2": "_t001", "-CK1": "_t001", "-CK2": "_t001", ".t01-CK" : "_t001", "-CK": "_t001",
              "-00005": "_t005", "-00006": "_t006",".t01": "_t001", ".t02": "_t002", ".t03": "_t003",
              ".t04": "_t004", ".t05": "_t005", ".t06": "_t006", "|" : "_", ":" : "_"}
              # if orig_id dosn't exist, I use the Name attribute to retrieve the good mRNA ID
              if 'orig_id' not in mRNA.attributes:
                # replacement/format
                for i, j in replacement.items():
                  mRNA.attributes['Name'] = mRNA.attributes['Name'][0].replace(i, j)
                # assignment of the new mRNA ID
                mRNA.attributes['ID']=mRNA.attributes['Name']
                if gene.attributes['ID'][0] not in mRNA.attributes['ID'][0]:
                  if (".2" not in gene.attributes['ID'][0]) and ("Vitvi" in mRNA.attributes['ID'][0]):
                    mRNA.attributes['ID']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}', gene.attributes['ID'][0], mRNA.attributes['ID'][0])
                    mRNA.attributes['Name']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}', gene.attributes['ID'][0], mRNA.attributes['Name'][0])
                  elif (".2" in gene.attributes['ID'][0]) and ("Vitvi" in mRNA.attributes['ID'][0]):
                    mRNA.attributes['ID']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}\.2', gene.attributes['ID'][0], mRNA.attributes['ID'][0])
                    mRNA.attributes['Name']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}\.2', gene.attributes['ID'][0], mRNA.attributes['Name'][0])
                  # there are mRNAs with a good gene name but the name of the transcript contains a lot of comments
                  # example : H_3-6kb_flnc_21588_C.mrna1
                  # so I replace this kind of ID with the ID of the gene + t00 + the number of the current RNA
                  elif "Vitvi" not in mRNA.attributes['ID'][0]:
                    mRNA.attributes['ID']= gene.attributes['ID'][0] + "_t00" + str(nb_mrna)
                    mRNA.attributes['Name']= gene.attributes['ID'][0] + "_t00" + str(nb_mrna)
                mRNA.attributes['Parent']=gene.attributes['ID']
              else :
                # replacement/format
                for i, j in replacement.items():
                  mRNA.attributes['orig_id'] = mRNA.attributes['orig_id'][0].replace(i, j)
                # assignment of the new mRNA ID and Name
                mRNA.attributes['ID']=mRNA.attributes['orig_id']
                mRNA.attributes['Name']=mRNA.attributes['orig_id']
                if gene.attributes['ID'][0] not in mRNA.attributes['ID'][0]:
                  if (".2" not in gene.attributes['ID'][0]) and ("Vitvi" in mRNA.attributes['ID'][0]):
                    mRNA.attributes['ID']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}', gene.attributes['ID'][0], mRNA.attributes['ID'][0])
                    mRNA.attributes['Name']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}', gene.attributes['ID'][0], mRNA.attributes['Name'][0])
                  elif (".2" in gene.attributes['ID'][0]) and ("Vitvi" in mRNA.attributes['ID'][0]):
                    mRNA.attributes['ID']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}\.2', gene.attributes['ID'][0], mRNA.attributes['ID'][0])
                    mRNA.attributes['Name']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}\.2', gene.attributes['ID'][0], mRNA.attributes['Name'][0])
                  elif "Vitvi" not in mRNA.attributes['ID'][0]:
                    mRNA.attributes['ID']= gene.attributes['ID'][0] + "_t00" + str(nb_mrna)
                    mRNA.attributes['Name']= gene.attributes['ID'][0] + "_t00" + str(nb_mrna)
                mRNA.attributes['Parent']=gene.attributes['ID']
              if change_mRNA_ID == "yes":
                replaced = re.sub('Vitvi[0-9]{2}[g][0-9]{5}', gene.attributes['ID'][0], mRNA.attributes['ID'][0])
                mRNA.attributes['ID']=replaced
              if mRNA.attributes['ID'][0] not in seen_mRNA:
                seen_mRNA[mRNA.attributes['ID'][0]] = 1
              else:
                seen_mRNA[mRNA.attributes['ID'][0]] += 1
              # imaginons qu'on ait 3 _t001, le deuxième deviendra t002 et le troisième deviendra t002 aussi si on ne gère
              # pas plusieurs itérations de vérification : c'est ce que je fais ici
              # c'était le cas de Vitvi04g00510 qui avait 3 t001 et générait un t001 et deux t002
              # maintenant ça génère t001, t002, t003 et ça peut générer t004, t005 etc grâce au while
              if seen_mRNA[mRNA.attributes['ID'][0]]>1:
                new_transcript_number=int(mRNA.attributes['ID'][0][-1])
                new_transcript_number+=1
                print(new_transcript_number)
                if mRNA.attributes['ID'][0][:-1] + str(new_transcript_number) not in seen_mRNA:
                  mRNA.attributes['ID']=mRNA.attributes['ID'][0][:-1] + str(new_transcript_number)
                  seen_mRNA[mRNA.attributes['ID'][0]] = 1
                else :
                  while mRNA.attributes['ID'][0][:-1] + str(new_transcript_number) in seen_mRNA:
                    print("Transcript number used yet, while to find the new transcript number")
                    print(mRNA.attributes['ID'][0][:-1] + str(new_transcript_number))
                    new_transcript_number+=1
                  mRNA.attributes['ID']=mRNA.attributes['ID'][0][:-1] + str(new_transcript_number)
                  print("New transcript ID ...")
                  print(mRNA.attributes['ID'])
                  seen_mRNA[mRNA.attributes['ID'][0]] = 1
              # if mRNA ID is the same as gene ID, I add _t00*
              # if _t00 exist, I add +1 to the transcript number
              # example : VVRGA10
              if mRNA.attributes['ID'][0] in seen_gene:
                if mRNA.attributes['ID'][0] + "_t00" + str(mRNA_transcript_number) not in seen_mRNA:
                  mRNA.attributes['ID']=mRNA.attributes['ID'][0] + "_t00" + str(mRNA_transcript_number)
                  seen_mRNA[mRNA.attributes['ID'][0]] = 1
                else :
                  print("Transcript number used yet, while to find the new transcript number")
                  print(mRNA.attributes['ID'][0] + "_t00" + str(mRNA_transcript_number))
                  while mRNA.attributes['ID'][0] + "_t00" + str(mRNA_transcript_number) in seen_mRNA:
                    mRNA_transcript_number+=1
                print("New transcript ID ...")
                mRNA.attributes['ID']=mRNA.attributes['ID'][0] + "_t00" + str(mRNA_transcript_number)
                print(mRNA.attributes['ID'])
                seen_mRNA[mRNA.attributes['ID'][0]] = 1
              mRNA.source="MANUAL_CURATION"
              if ('status' in mRNA.attributes):
                if (("Approved" in gene.attributes['status']) and ("To delete" not in mRNA.attributes['status'])) :
                  output_file.write("{}\n".format(str(mRNA)))
                  count_exon=1
                  count=0
                  # Apollo don't manage the UTRs
                  # sometimes it creates a exon without a CDS because this is entirely an UTR
                  count_cds=0
                  for exon_cds in sorted(db_apollo.children(mRNA.attributes['apollo_id'][0], level=1),key=lambda y: (y[3], reversor(y[2]), y[4])):
                    count+=1
                    if exon_cds.featuretype == "exon":
                      exon_cds.attributes['ID']=str(mRNA.attributes['ID'][0]) + ".exon" + str(count_exon)
                      exon_cds.attributes['Name']=str(mRNA.attributes['ID'][0]) + ".exon" + str(count_exon)
                      exon_cds.attributes['Parent']=str(mRNA.attributes['ID'][0])
                      # if exon place is peer, it's because the previous exon don't have a cds (it's an UTR)
                      count_exon+=1
                      if (count % 2) != 0:
                        count_cds=1
                      exon_cds.source="MANUAL_CURATION"
                      output_file.write("{}\n".format(str(exon_cds)))
                    elif exon_cds.featuretype == "CDS":
                      exon_cds.attributes['ID']=mRNA.attributes['ID'][0] + ".cds" + str(count_exon-count_cds)
                      exon_cds.attributes['Name']=mRNA.attributes['ID'][0] + ".cds" + str(count_exon-count_cds)
                      exon_cds.attributes['Parent']=mRNA.attributes['ID'][0]
                      exon_cds.source="MANUAL_CURATION"
                      output_file.write("{}\n".format(str(exon_cds)))
                elif ("To delete" in mRNA.attributes['status']):
                  output_delete_file.write("{}\n".format(str(mRNA.attributes['ID'][0])))
              elif ("Approved" in gene.attributes['status']):
                output_file.write("{}\n".format(str(mRNA)))
                count_exon=1
                count=0
                # Apollo don't manage the UTRs
                # sometimes it creates a exon without a CDS because this is entirely an UTR
                count_cds=0
                for exon_cds in sorted(db_apollo.children(mRNA.attributes['apollo_id'][0], level=1),key=lambda y: (y[3], reversor(y[2]), y[4])):
                  count+=1
                  if exon_cds.featuretype == "exon":
                    exon_cds.attributes['ID']=str(mRNA.attributes['ID'][0]) + ".exon" + str(count_exon)
                    exon_cds.attributes['Name']=str(mRNA.attributes['ID'][0]) + ".exon" + str(count_exon)
                    exon_cds.attributes['Parent']=str(mRNA.attributes['ID'][0])
                    # if exon place is peer, it's because the previous exon don't have a cds (it's an UTR)
                    count_exon+=1
                    if (count % 2) != 0:
                      count_cds=1
                    exon_cds.source="MANUAL_CURATION"
                    output_file.write("{}\n".format(str(exon_cds)))
                  elif exon_cds.featuretype == "CDS":
                    exon_cds.attributes['ID']=mRNA.attributes['ID'][0] + ".cds" + str(count_exon-count_cds)
                    exon_cds.attributes['Name']=mRNA.attributes['ID'][0] + ".cds" + str(count_exon-count_cds)
                    exon_cds.attributes['Parent']=mRNA.attributes['ID'][0]
                    exon_cds.source="MANUAL_CURATION"
                    output_file.write("{}\n".format(str(exon_cds)))
        #----------------------------------------------------------------
        ###### JUST THE PART FOR MRNAs APPROVED AND GENE WITH NO STATUS : beginning
        #----------------------------------------------------------------
        elif mRNA_approved_but_gene_no_status=="YES":
          print("This is a gene with no status but mRNA approved")
          print(gene.attributes['Name'])
          # some gene Names attribute are Vitvi*g0****, others are Vitvi*g0****.t01, others Vitvi*g0****_t001
          # we have to correct that at the gene level
          # If the gene Name contains Vitvi, The Vitvi**g0**** good format has 13 characters, so we keep only
          # the first 13 characters
          # else if gene ID is not a Vitvi, like WRKY45, we keep it as it is.
          # Later in the script we will try to blast these type of genes and find a Vitvi ID for them
          gene.attributes['apollo_id']=gene.attributes['ID']
          if "Vitvi" in gene.attributes['Name'][0]:
            gene.attributes['Name']=gene.attributes['Name'][0][ 0 : 13 ]
          gene.attributes['ID']=gene.attributes['Name'][0].replace("|", "_").replace(":", "_")
          replacement_gene={".t01": "", "_t001" : "", "_t002" : "", ".t02": "", ".t03": ""}
          for i, j in replacement_gene.items():
            gene.attributes['ID'] = gene.attributes['ID'][0].replace(i, j)
            gene.attributes['Name'] = gene.attributes['Name'][0].replace(i, j)
          allTheGenes.append(gene.attributes['Name'][0])
          # sometimes the gene ID is the same for two different genes,
          # example : Vitvi18g03067
          # so I created seen_gene object containing the gene ID and when there is a duplicate, I add .2 at the end of the ID
          # for example I got Vitvi18g03067 and Vitvi18g03067.2
          # but I don't change the Name, but the ID must to be unique
          if gene.attributes['ID'][0] not in seen_gene:
            seen_gene[gene.attributes['ID'][0]] = 1
          else:
            seen_gene[gene.attributes['ID'][0]] += 1
          if seen_gene[gene.attributes['ID'][0]]>1:
            change_mRNA_ID="yes"
            print("This gene is a split gene :", gene.attributes['ID'])
            print("For now I just add '.2', '.3' etc ... for each part of the split gene.")
            gene.attributes['ID']=gene.attributes['ID'][0] + "." + str(seen_gene[gene.attributes['ID'][0]])
          gene.source="MANUAL_CURATION"
          remove_gene=""
          nb_mRNAs=sum(1 for dummy in db_apollo.children(gene.id, level=1))
          for mRNA in db_apollo.children(gene.id, level=1):
            if ('status' in mRNA.attributes):
              if ("To delete" in mRNA.attributes['status'] and nb_mRNAs == 1):
                remove_gene="yes"
          if remove_gene != "yes" :
            output_file.write("{}\n".format(str(gene)))
          else:
            output_delete_file.write("{}\n".format(str(gene.attributes['ID'][0])))
          mRNA_transcript_number=1
          nb_mrna=0
          for mRNA in db_apollo.children(gene.id, level=1):
            nb_mrna+=1
            # I keep Apollo ID in a new attribute
            mRNA.attributes['apollo_id']=mRNA.attributes['ID']
            # I keep Apollo Name in a new attribute
            mRNA.attributes['apollo_name']=mRNA.attributes['Name']
            # keep the symbol as attribute to show it on Apollo
            if 'symbol' in gene.attributes:
              if 'Note' in gene.attributes:
                mRNA.attributes['Note']=gene.attributes['symbol']
              else:
                mRNA.attributes['description']=gene.attributes['symbol']
            # dictionnary of replacement
            # we have to add a note in the guidelines about the mRNA naming
            replacement={".t01-00001": "_t001",".t01a-00001": "_t001",".t01-00002": "_t001",".t01-00003": "_t001",
            "-MK_TEST" :"_t001","-CK_TEST" :"_t001","a-CK":"_t001",
            "-00001": "_t001", "-00002": "_t002", "-00003": "_t003", "-00004": "_t004", ".t01-CK1": "_t001",
            ".t01-CK2": "_t001", "-CK1": "_t001", "-CK2": "_t001", ".t01-CK" : "_t001", "-CK": "_t001",
            "-00005": "_t005", "-00006": "_t006",".t01": "_t001", ".t02": "_t002", ".t03": "_t003",
            ".t04": "_t004", ".t05": "_t005", ".t06": "_t006", "|" : "_", ":" : "_"}
            # if orig_id dosn't exist, I use the Name attribute to retrieve the good mRNA ID
            if 'orig_id' not in mRNA.attributes:
              # replacement/format
              for i, j in replacement.items():
                mRNA.attributes['Name'] = mRNA.attributes['Name'][0].replace(i, j)
              # assignment of the new mRNA ID
              mRNA.attributes['ID']=mRNA.attributes['Name']
              if gene.attributes['ID'][0] not in mRNA.attributes['ID'][0]:
                if (".2" not in gene.attributes['ID'][0]) and ("Vitvi" in mRNA.attributes['ID'][0]):
                  mRNA.attributes['ID']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}', gene.attributes['ID'][0], mRNA.attributes['ID'][0])
                  mRNA.attributes['Name']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}', gene.attributes['ID'][0], mRNA.attributes['Name'][0])
                elif (".2" in gene.attributes['ID'][0]) and ("Vitvi" in mRNA.attributes['ID'][0]):
                  mRNA.attributes['ID']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}\.2', gene.attributes['ID'][0], mRNA.attributes['ID'][0])
                  mRNA.attributes['Name']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}\.2', gene.attributes['ID'][0], mRNA.attributes['Name'][0])
                # there are mRNAs with a good gene name but the name of the transcript contains a lot of comments
                # example : H_3-6kb_flnc_21588_C.mrna1
                # so I replace this kind of ID with the ID of the gene + t00 + the number of the current RNA
                elif "Vitvi" not in mRNA.attributes['ID'][0]:
                  mRNA.attributes['ID']= gene.attributes['ID'][0] + "_t00" + str(nb_mrna)
                  mRNA.attributes['Name']= gene.attributes['ID'][0] + "_t00" + str(nb_mrna)
              mRNA.attributes['Parent']=gene.attributes['ID']
            else :
              # replacement/format
              for i, j in replacement.items():
                mRNA.attributes['orig_id'] = mRNA.attributes['orig_id'][0].replace(i, j)
              # assignment of the new mRNA ID and Name
              mRNA.attributes['ID']=mRNA.attributes['orig_id']
              mRNA.attributes['Name']=mRNA.attributes['orig_id']
              if gene.attributes['ID'][0] not in mRNA.attributes['ID'][0]:
                if (".2" not in gene.attributes['ID'][0]) and ("Vitvi" in mRNA.attributes['ID'][0]):
                  mRNA.attributes['ID']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}', gene.attributes['ID'][0], mRNA.attributes['ID'][0])
                  mRNA.attributes['Name']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}', gene.attributes['ID'][0], mRNA.attributes['Name'][0])
                elif (".2" in gene.attributes['ID'][0]) and ("Vitvi" in mRNA.attributes['ID'][0]):
                  mRNA.attributes['ID']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}\.2', gene.attributes['ID'][0], mRNA.attributes['ID'][0])
                  mRNA.attributes['Name']=re.sub('Vitvi[0-9]{2}[g][0-9]{5}\.2', gene.attributes['ID'][0], mRNA.attributes['Name'][0])
                elif "Vitvi" not in mRNA.attributes['ID'][0]:
                  mRNA.attributes['ID']= gene.attributes['ID'][0] + "_t00" + str(nb_mrna)
                  mRNA.attributes['Name']= gene.attributes['ID'][0] + "_t00" + str(nb_mrna)
              mRNA.attributes['Parent']=gene.attributes['ID']
            if change_mRNA_ID == "yes":
              replaced = re.sub('Vitvi[0-9]{2}[g][0-9]{5}', gene.attributes['ID'][0], mRNA.attributes['ID'][0])
              mRNA.attributes['ID']=replaced
            if mRNA.attributes['ID'][0] not in seen_mRNA:
              seen_mRNA[mRNA.attributes['ID'][0]] = 1
            else:
              seen_mRNA[mRNA.attributes['ID'][0]] += 1
            # imaginons qu'on ait 3 _t001, le deuxième deviendra t002 et le troisième deviendra t002 aussi si on ne gère
            # pas plusieurs itérations de vérification : c'est ce que je fais ici
            # c'était le cas de Vitvi04g00510 qui avait 3 t001 et générait un t001 et deux t002
            # maintenant ça génère t001, t002, t003 et ça peut générer t004, t005 etc grâce au while
            if seen_mRNA[mRNA.attributes['ID'][0]]>1:
              new_transcript_number=int(mRNA.attributes['ID'][0][-1])
              new_transcript_number+=1
              print(new_transcript_number)
              if mRNA.attributes['ID'][0][:-1] + str(new_transcript_number) not in seen_mRNA:
                mRNA.attributes['ID']=mRNA.attributes['ID'][0][:-1] + str(new_transcript_number)
                seen_mRNA[mRNA.attributes['ID'][0]] = 1
              else :
                while mRNA.attributes['ID'][0][:-1] + str(new_transcript_number) in seen_mRNA:
                  print("Transcript number used yet, while to find the new transcript number")
                  print(mRNA.attributes['ID'][0][:-1] + str(new_transcript_number))
                  new_transcript_number+=1
                mRNA.attributes['ID']=mRNA.attributes['ID'][0][:-1] + str(new_transcript_number)
                print("New transcript ID ...")
                print(mRNA.attributes['ID'])
                seen_mRNA[mRNA.attributes['ID'][0]] = 1
            # if mRNA ID is the same as gene ID, I add _t00*
            # if _t00 exist, I add +1 to the transcript number
            # example : VVRGA10
            if mRNA.attributes['ID'][0] in seen_gene:
              if mRNA.attributes['ID'][0] + "_t00" + str(mRNA_transcript_number) not in seen_mRNA:
                mRNA.attributes['ID']=mRNA.attributes['ID'][0] + "_t00" + str(mRNA_transcript_number)
                seen_mRNA[mRNA.attributes['ID'][0]] = 1
              else :
                print("Transcript number used yet, while to find the new transcript number")
                print(mRNA.attributes['ID'][0] + "_t00" + str(mRNA_transcript_number))
                while mRNA.attributes['ID'][0] + "_t00" + str(mRNA_transcript_number) in seen_mRNA:
                  mRNA_transcript_number+=1
              print("New transcript ID ...")
              mRNA.attributes['ID']=mRNA.attributes['ID'][0] + "_t00" + str(mRNA_transcript_number)
              print(mRNA.attributes['ID'])
              seen_mRNA[mRNA.attributes['ID'][0]] = 1
            mRNA.source="MANUAL_CURATION"
            if ('status' in mRNA.attributes):
              if (("Approved" in mRNA.attributes['status'])) :
                output_file.write("{}\n".format(str(mRNA)))
                count_exon=1
                count=0
                # Apollo don't manage the UTRs
                # sometimes it creates a exon without a CDS because this is entirely an UTR
                count_cds=0
                for exon_cds in sorted(db_apollo.children(mRNA.attributes['apollo_id'][0], level=1),key=lambda y: (y[3], reversor(y[2]), y[4])):
                  count+=1
                  if exon_cds.featuretype == "exon":
                    exon_cds.attributes['ID']=str(mRNA.attributes['ID'][0]) + ".exon" + str(count_exon)
                    exon_cds.attributes['Name']=str(mRNA.attributes['ID'][0]) + ".exon" + str(count_exon)
                    exon_cds.attributes['Parent']=str(mRNA.attributes['ID'][0])
                    # if exon place is peer, it's because the previous exon don't have a cds (it's an UTR)
                    count_exon+=1
                    if (count % 2) != 0:
                      count_cds=1
                    exon_cds.source="MANUAL_CURATION"
                    output_file.write("{}\n".format(str(exon_cds)))
                  elif exon_cds.featuretype == "CDS":
                    exon_cds.attributes['ID']=mRNA.attributes['ID'][0] + ".cds" + str(count_exon-count_cds)
                    exon_cds.attributes['Name']=mRNA.attributes['ID'][0] + ".cds" + str(count_exon-count_cds)
                    exon_cds.attributes['Parent']=mRNA.attributes['ID'][0]
                    exon_cds.source="MANUAL_CURATION"
                    output_file.write("{}\n".format(str(exon_cds)))
              elif ("To delete" in mRNA.attributes['status']):
                output_delete_file.write("{}\n".format(str(mRNA.attributes['ID'][0])))
        #----------------------------------------------------------------
        ###### JUST THE PART FOR MRNAs APPROVED AND GENE WITH NO STATUS : END
        #----------------------------------------------------------------
      except KeyError:
        pass
    change_mRNA_ID="no"
  output_file.close()
  print("Number of 'Approved' and 'To delete' genes to process: ", len(list(set(allTheGenes))))
  print("List of these genes : ")
  print(list(set(allTheGenes)))
  print("------------------")
  return list(set(allTheGenes))

# here, we determine which genes from the new formatted gff3 should be blasted + the genes with "g04...." ID from PN40024.v4.1
# pseudogene will not be blasted, because they have no CDS, so no proteins, just transcript and exons. So, the pseudogenes will have a "04" gene ID, automatically.
def gene_sequences_to_blast(foutput,ref_fasta,db_ref, db_apollo_formatted,ffasta, fasta_cds_REF,fref):
  print("------------------")
  print("Finding gene sequences to blast ...")
  name_fasta_CDS= str(foutput)+ 'cds.fasta'
  # -y write a protein fasta file with the translation of CDS for each record
  gffread_command = "gffread -x {0} -g {1} {2}; sed -i 's/|/_/g' {0}".format(name_fasta_CDS, ref_fasta, foutput)
  print("Retrieving cds sequences ...")
  print(gffread_command)
  os.system(gffread_command)
  # for pseudogene, that are not retrieve with the proteins command
  # -nc non-coding only: discard mRNAs that have CDS features -> so we work only on pseudogenes which dont have cds
  # -w write a fasta file with spliced exons for each GFF transcript
  name_fasta_exon_pseudogenes= str(foutput)+ 'exons_pseudogenes.fasta'
  gffread_command_pseudo = "gffread --nc -w {0} -g {1} {2}; sed -i 's/|/_/g' {0}".format(name_fasta_exon_pseudogenes, ref_fasta, foutput)
  print("Retrieving exon sequences for pseudogenes ...")
  print(gffread_command_pseudo)
  os.system(gffread_command_pseudo)
  fasta_final=str(foutput)+ 'cds_genes_and_exons_pseudogenes.fasta'
  merge_cds_gene_and_exons_pseudogenes="cat {0} {1} > {2}".format(name_fasta_CDS, name_fasta_exon_pseudogenes, fasta_final)
  print("Merge cds sequences for genes and exon sequences for pseudogenes ...")
  print(merge_cds_gene_and_exons_pseudogenes)
  os.system(merge_cds_gene_and_exons_pseudogenes)
  db_ref = gffutils.FeatureDB(db_ref)
  db_apollo_updated = gffutils.FeatureDB(db_apollo_formatted)
  mRNAs_apollo=[]
  mRNAs_ref=[]
  # in apollo gff3 file there are genes and pseudogenes
  # genes have mRNAs and pseudogenes have transcripts type of children
  # the ref file (PN40024.v4.1 doesn't has pseudogene as featuretype, so no need to chain the iterators)
  mRNAs_and_transcripts_iterator=chain(db_apollo_updated.features_of_type('mRNA'), db_apollo_updated.features_of_type('transcript'))
  for mRNA in mRNAs_and_transcripts_iterator:
    mRNAs_apollo.append(mRNA.attributes['ID'][0])
  for mRNA in db_ref.features_of_type('mRNA'):
    mRNAs_ref.append(mRNA.attributes['ID'][0])
  # for the genes to blast on PN12xV2 we take all the Apollo genes not found in PN40024.v4.1 -> the new genes created in Apollo
  genelist_to_blast = np.setdiff1d(mRNAs_apollo,mRNAs_ref).tolist()
  print('Number of mRNAs not found in PN40024.v4 REF file (new Apollo mRNAs/genes) :', len(genelist_to_blast))
  print(genelist_to_blast)
  # but we also take all the "g04...." genes in PN40024.v4.1 to try to give them a PN12Xv2 VCostV3 ID
  regexp = re.compile(r'Vitvi[0-9]{2}[g]04[0-9]{3}')
  for mRNA in db_ref.features_of_type('mRNA'):
    if regexp.search(mRNA.attributes['ID'][0]):
      genelist_to_blast.append(mRNA.attributes['ID'][0])
  # we only keep unique items in the list, so unique mRNA IDs
  genelist_to_blast_OK=list(set(genelist_to_blast))
  print('Number of mRNAs not found in PN40024.v4 REF file (new Apollo genes) and the "04" IDs in PN40024.v4 REF :', len(genelist_to_blast_OK))
  print(genelist_to_blast)
  with open('genes_to_blast.txt', 'w') as f:
    for item in genelist_to_blast_OK:
      f.write("%s " % item)
  print("So, there are ", len(genelist_to_blast_OK), "genes to blast.")
  sed_command = "sed -i 's/|/_/g' genes_to_blast.txt"
  tmp_txt="TMP/genes_to_blast_final.txt"
  os.system(sed_command)
  # here we create a fasta file containing all the Apollo sequences and all the REF sequences which are different from those curated and approved in Apollo
  # donc on parcours le fichier ref, puis le fichier apollo, si l'ID ref est dans apollo, on prend la séquence apollo, sinon on prend la séquence ref
  # So we loop through the PN40024.v4.1 file, then the Apollo file, if the PN40024.v4.1 ID is in Apollo, we take the Apollo sequence, otherwise we take the PN40024.v4.1 sequence
  # fasta_final : Apollo's curated cds (for genes) or exons (for pseudogenes)
  # fasta_cds_REF : the cds sequences in PN40024.v4.1
  # here I put all the IDs of the Apollo fasta cds in a list
  record_ids = []
  sequences = []
  for record in SeqIO.parse(fasta_final, 'fasta'):
    record_ids.append(record.id)
  # ici, tous les IDs des gènes curés dans Apollo qui sont présent dans PN40024.v4.1 seront supprimés des gènes à blaster de PN40024.v4.1 car on veut garder la version curé
  # mais parfois c'est un VIT_ qui a été utilisé pour la curation, exemple VIT_08s0007g00670 et donc le gène dans PN40024.v4.1 qui s'appelle vitvi08g02201 n'est pas supprimé pour la recherche de RBH
  # forcément, contre VCOStv3, c'est vitvi08g02201 qui a le best hit contre le vitvi08g02201 de vcostv3 et non VIT_08s0007g00670, donc VIT_08s0007g00670 est renommé en 4000
  # seulement après, par intersection, vitvi08g02201 est supprimé donc il faut le supprimer directement des gènes à blaster dans PN40024.v4.1. En janvier 2022, il y avait 22 cas comme ça.
  def insertion_filter(feature):
    return 'gene' in feature[2]
  a = pybedtools.BedTool(foutput).filter(insertion_filter)
  b = pybedtools.BedTool(fref).filter(insertion_filter)
  intersection_VIT=a.intersect(b,s=True, f=0.8, nonamecheck=True,wo=True)
  if os.path.isfile('tmp_intersect_VIT.gff3'):
    os.remove('tmp_intersect_VIT.gff3')
  with open('tmp_intersect_VIT.gff3', 'w') as tmpfile2:
    tmpfile2.write(str(intersection_VIT))
  if os.path.getsize('tmp_intersect_VIT.gff3') > 0:
    intersection_VIT_dataframe=pd.read_table('tmp_intersect_VIT.gff3',header=None)
  for i in range(len(intersection_VIT_dataframe)) :
    ID_apollo=str(intersection_VIT_dataframe.iloc[i, 8])
    ID_apollo=re.sub(".*ID=","",ID_apollo)
    ID_apollo=re.sub(";.*","",ID_apollo)
    ID_ref=str(intersection_VIT_dataframe.iloc[i, 17]).replace(".*ID=","").replace(";.*","")
    ID_ref=re.sub(".*ID=","",ID_ref)
    ID_ref=re.sub(";.*","",ID_ref)
    if ((ID_apollo != ID_ref) and ("Vitvi" not in ID_apollo)):
      ID_mRNA1=str(ID_ref) + "_t001"
      ID_mRNA2=str(ID_ref) + "_t002"
      ID_mRNA3=str(ID_ref) + "_t003"
      ID_mRNA4=str(ID_ref) + "_t004"
      ID_mRNA5=str(ID_ref) + "_t005"
      ID_mRNA6=str(ID_ref) + "_t006"
      ID_mRNA7=str(ID_ref) + "_t007"
      record_ids.append(ID_mRNA1)
      record_ids.append(ID_mRNA2)
      record_ids.append(ID_mRNA3)
      record_ids.append(ID_mRNA4)
      record_ids.append(ID_mRNA5)
      record_ids.append(ID_mRNA6)
      record_ids.append(ID_mRNA7)
  print("Sequence IDs to remove from PN40024.v4.1 for RBH search :")
  print(record_ids)
  # all PN40024.v4.1 cds that are not in this Apollo ID list are kept, the others are not
  for record_ref in SeqIO.parse(fasta_cds_REF, 'fasta'):
    if record_ref.id not in record_ids:
      sequences.append(record_ref)
  os.remove('tmp_intersect_VIT.gff3')
  # then I get the cds sequences of Apollo then the cds of PN40024.v4.1, and I get a full fasta of the
  # cds with the curated + all others PN40024.v4.1 cds for the RBH step
  for record_apollo in SeqIO.parse(fasta_final, 'fasta'):
    sequences.append(record_apollo)
  SeqIO.write(sequences, ffasta, 'fasta')
  # here we keep only the longest transcript in ffasta because if there is multiple transcripts with a rbh on vcostv3
  # transcripts, then the vcostv3 has multiple rbh on the ffasta sequences and then the sequences is not renamed
  # because we need only ONE rbh
  longest_transcript_file=str(ffasta) + ".longest_transcript.txt"
  ids_before_longest=[]
  for record in SeqIO.parse(ffasta, 'fasta'):
    ids_before_longest.append(record.id)
  if os.path.isfile(longest_transcript_file):
    print("longest_transcript_file exist, skip the step of selection")
  else :
    longest_transcript_extraction_command="./select_longest_transcript.sh {0} {1}".format(ffasta, longest_transcript_file)
    print("Extract longest transcript for each gene ...")
    print(longest_transcript_extraction_command)
    os.system(longest_transcript_extraction_command)
  ids_after_longest=[]
  for record in SeqIO.parse(ffasta, 'fasta'):
    ids_after_longest.append(record.id)
  df=open('IDs_smallest_transcript_to_remove.txt','w')
  for i in np.setdiff1d(ids_before_longest,ids_after_longest):
    df.write(str(i))
    df.write("\n")
  df.close()
  ## ffasta is the final fasta with all the new/updated Apollo cds + the other PN40024.v4.1 cds not processed in Apollo
  # it will be used to reciprocal best hit of all PN40024.v4.1 cds against all PN12Xv2 cds
  # and that will then allow to change the ID of the genes to blast, those which did not exist in PN40024.v4.1 + those which exist in PN40024.v4.1 but with an ID "g04...."
  os_command3="tr ' ' '\n' < {0} > {1}".format("genes_to_blast.txt",tmp_txt)
  print("Retrieving genes IDs to blast ...")
  print(os_command3)
  os.system(os_command3)
  tmp_file=str(tmp_txt) + ".tmp"
  grep_command="grep -Fvxf IDs_smallest_transcript_to_remove.txt {0} > {1}; mv {1} {0}".format(tmp_txt,tmp_file)
  print("Remove smallest mRNAs ...")
  print(grep_command)
  os.system(grep_command)
  os.remove("genes_to_blast.txt")
  os.remove(name_fasta_CDS)
  os.remove(fasta_final)
  os.remove("IDs_smallest_transcript_to_remove.txt")
  os.remove(name_fasta_exon_pseudogenes)
  print("------------------")

# here we make the RBH of PN40024.v4.1 + Apollo proteins against PN12Xv2 VCost v3 proteins
# pseudogene are not in the PN40024.v4.1 proteins, because they have no CDS, just transcript and exons. So, the pseudogenes will have a "04" gene ID, automatically.
def make_blast_RBH(ffasta, blastoutput, blast_database):
  print("------------------")
  print("Make blast of", ffasta, "against", blast_database, "...")
  if os.path.isfile(blastoutput):
    print(blastoutput," file exists, so we don't recreate it")
    pass
  else:
    # blast reciprocal best hit between apollo + PN40024.v4.1 proteins and vitviv2.pep.fasta (PN12Xv2)
    blast_rbh_command = "python blast_rbh.py --threads 10 -a nucl -t blastn -o {2} {0} {1}".format(ffasta,blast_database, blastoutput)
    print("Finding reciprocal best hits between", ffasta, "and", blast_database)
    print(blast_rbh_command)
    os.system(blast_rbh_command)
    # this blast RBH takes 1h30 to run with 10 CPUs
  num_lines = sum(1 for line in open(blastoutput)) -1
  print("Number of Reciprocal Best Hits (RBH) found :", num_lines, "for the all vs all genes of PN40024.v4 and PN12Xv2. I will used these RBH results to see if the 'genes to blast' have a RBH.")
  print("------------------")

# here we create the dictionnary for renaming gene and the dictionnary containing the last "g04...." ID for each chromosome
def rename_genes(foutput, db_ref, db_apollo_formatted,blastoutput,db_oldgff3):
  print("------------------")
  print("Renaming the genes with their RBH in PN12X or, for new genes without a match on PN12X, renaming them with a '04' ID...")
  print("--------------------")
  print("Create the dictionnary of used Vitvi04 and keep the first to use for each chromosomes")
  # use of db_oldgff3 to keep same vitvi04 ID as before for gene without RBH
  Vitvi04_used_dict={}
  db_oldgff3 = gffutils.FeatureDB(db_oldgff3)
  genes_and_pseudogenes_iterator_oldgff3=chain(db_oldgff3.features_of_type('gene'), db_oldgff3.features_of_type('pseudogene'))
  # si dans le gff3 précédent un gène est vitvi04 et qu'il a un alias_before_renaming c'est qu'il a été renommé en vitvi04
  # donc on doit garder le même vitvi04 pour cet alias_before_renaming
  for gene in genes_and_pseudogenes_iterator_oldgff3:
    if "g04" in gene.attributes['ID'][0]:
      if 'alias_before_renaming' in gene.attributes:
        Vitvi04_used_dict[gene.attributes['alias_before_renaming'][0]] = gene.attributes['ID'][0]
  print("Vitvi04_used_dict dictionnary !")
  print(Vitvi04_used_dict)
  genes_renaming_dict={}
  db_ref = gffutils.FeatureDB(db_ref)
  db_apollo_updated = gffutils.FeatureDB(db_apollo_formatted)
  tmp_txt="TMP/genes_to_blast_final.txt"
  with open(tmp_txt) as file_in2:
    genes_not_found_PN40024_v4 = []
    for line in file_in2:
      genes_not_found_PN40024_v4.append(line.strip())
  list_replacement={}
  list_no_replacement={}
  # here we will rename the genes of the list genes_not_found_PN40024_v4 according to their RBH found
  # genes_not_found_PN40024_v4 contains the gene IDs of the new/curated Apollo genes + the genes in PN40024.v4.1 which have a "g04...." ID
  with open(blastoutput) as file_in:
    count=0
    genes_with_a_RBH = []
    for line in file_in:
      if count == 0:
        print("pass header...")
      else:
        print("Work on blastoutput...")
        items=line.strip().split('\t')
        PN40024v4_geneID=re.sub('_t[0-9]{3}','',items[0])
        print("PN40024v4_geneID:",PN40024v4_geneID)
        PN40024v4_ID=items[0]
        print("PN40024v4_ID:",PN40024v4_ID)
        PN12X_geneID=re.sub('\.t[0-9]{2}','',items[1])
        print("PN12X_geneID:",PN12X_geneID)
        # we change the gene ID only if apollo_id is different of the PN12xv2 gene ID,
        # because I want to rename all the genes not found in PN40024.v4.1 and give them a PN12XV2 gene ID but generally this new gene is created from PN12xV2 track
        # so the gene ID is the good yet
        if PN40024v4_geneID != PN12X_geneID:
          # we change the gene ID only if apollo_id is in the list of the genes_not_found_PN40024_v4
          if PN40024v4_ID in genes_not_found_PN40024_v4:
            # when there is two versions (or more) of a gene, I put a ".2" or more at the end
            # and for now I let this gene like that because else this replacement remove the ".2" and I finish with duplicated gene IDs as at the beginning
            if ".2" not in PN40024v4_ID:
            # warning, to change the ID of PN40024 by that of PN12Xv2, it must be on the same chromosome
            # if it is not on the same chromosome, the gene is not renamed and it remains in "g04...." format
              if "Vitvi" in PN40024v4_geneID:
                chrom_PN40024_gene=re.sub('[g][0-9]{5}','',PN40024v4_geneID)
                chrom_PN40024_gene = re.sub('Vitvi','chr',chrom_PN40024_gene)
                chrom_PN12X_gene=re.sub('[g][0-9]{5}','',PN12X_geneID)
                chrom_PN12X_gene = re.sub('Vitvi','chr',chrom_PN12X_gene)
              elif "VIT_" in PN40024v4_geneID:
                chrom_PN40024_gene=re.sub('[s][0-9]{4}[g][0-9]{5}','',PN40024v4_geneID)
                chrom_PN40024_gene = re.sub('VIT_','chr',chrom_PN40024_gene)
                chrom_PN12X_gene=re.sub('[g][0-9]{5}','',PN12X_geneID)
                chrom_PN12X_gene = re.sub('Vitvi','chr',chrom_PN12X_gene)
              else:
                feature_tmp=db_apollo_updated[PN40024v4_geneID]
                chrom_PN40024_gene=feature_tmp.chrom
                chrom_PN12X_gene=re.sub('[g][0-9]{5}','',PN12X_geneID)
                chrom_PN12X_gene = re.sub('Vitvi','chr',chrom_PN12X_gene)
              if chrom_PN40024_gene == chrom_PN12X_gene:
                genes_renaming_dict[PN40024v4_geneID] = PN12X_geneID
                genes_with_a_RBH.append(PN40024v4_ID)
                list_replacement[PN40024v4_geneID] = PN12X_geneID
              else:
                list_no_replacement[PN40024v4_geneID] = PN12X_geneID
                pass
        else:
          print(PN40024v4_geneID,"=",PN12X_geneID, "so it's a rbh")
          # if PN40024v4_geneID == PN12X_geneID
          # we add this gene as having an RBH, since it already has the correct ID
          # so it will not be taken into account to receive an ID of type "g04...."
          genes_with_a_RBH.append(PN40024v4_ID)

      count+=1
    print("Genes (",len(list_replacement), "total genes) with an RBH on PN12xV2 'PN40024.v4 ID' : 'PN12xV2 ID'")
    print(list_replacement)
    print("Genes (",len(list_no_replacement), "total genes) with  an RBH on PN12xV2 'PN40024.v4 ID' : 'PN12xV2 ID', but not on the same chromosome, so there is no replacement for these gene IDs")
    print(list_no_replacement)
    # if there is no RBH between a gene and PN12Xv2 I give it a "g04...." gene ID, the last for the corresponding chromosome

    # create a dictionnary with the last gene IDs '04' for each chromosome
    last_ID = []
    seen_gene_04 = {}

    genes_and_pseudogenes_iterator_apollo=chain(db_apollo_updated.features_of_type('gene'), db_apollo_updated.features_of_type('pseudogene'))
    for gene in genes_and_pseudogenes_iterator_apollo:
      last_ID.append([gene.chrom,gene.attributes['ID'][0]])
      seen_gene_04[gene.attributes['ID'][0]] = 1

    for gene in db_ref.features_of_type('gene'):
      if gene.attributes['ID'][0] not in seen_gene_04:
        last_ID.append([gene.chrom,gene.attributes['ID'][0]])
    chromosome = ["chr01","chr02","chr03","chr04","chr05","chr06","chr07","chr08","chr09","chr10","chr11","chr12","chr13","chr14","chr15","chr16","chr17","chr18","chr19","chrUn"]

    dict_vitvi04_IDs={}
    # we go through the list of lists, each list containing the chromosome and the gene ID
    # the goal being to find the largest ID "04" of each chromosome
    for chr in chromosome:
      chr_number= re.sub('chr','',chr)
      if chr == "chrUn":
        chr_number="00"
      tmp_list = []
      for sublist in last_ID:
        if sublist[0] == chr:
          if "g04" in str(sublist[1]):
            string = "Vitvi" + str(chr_number)
            if string in str(sublist[1]):
              tmp_list.append(sublist[1])
      tmp_list.sort(reverse=True)
      if not tmp_list:
        if chr == "chrUn":
          chr_number = "00"
        geneID = "Vitvi" + chr_number + "g04000"
        dict_vitvi04_IDs[chr] = geneID
      else:
        # we take the largest ID "04" and add +1 to find the last ID to use
        gene_number=int(re.sub("Vitvi[0-9]{2}[g]0", "", tmp_list[0]))
        gene_number+=1
        string= "g0" + str(gene_number)
        dict_vitvi04_IDs[chr]=re.sub("[g][0-9]{5}", string, tmp_list[0])
    print("-----------------------------------")
    print("Creation of a dictionary with the last '04' ID for each chromosome :")
    print(dict_vitvi04_IDs)

    # if there is no RBH for a gene, we give to him the last g04.... ID and increment this ID for the next gene without RBH on this chromosome
    list_no_RBH={}
    print("List of genes with no RBH, renamed with a 04 ID :")
    #print(np.setdiff1d(genes_not_found_PN40024_v4,genes_with_a_RBH))
    for gene in np.setdiff1d(genes_not_found_PN40024_v4,genes_with_a_RBH):
      print(gene)
      gene=re.sub('_t[0-9]{3}','',gene)
      if "Vitvi" in gene:
        if "g04" not in gene:
          print(gene)
          chrom_number=re.sub("Vitvi", "", gene)
          chrom_number=re.sub("[g][0-9]{5}", "", chrom_number)
          chrom_number=re.sub("\.2", "", chrom_number)
          #chrom_number=re.sub("_prot1_STS10_new", "", chrom_number)
          chrom_number=chrom_number.split("_",1)[0]
          chrom_name=re.sub("Vitvi", "chr", gene)
          chrom_name=re.sub("[g][0-9]{5}.*", "", chrom_name)
          chrom_name=re.sub("\.2", "", chrom_name)
          chrom_name=chrom_name.split("_",1)[0]
          #chrom_name=re.sub("_prot1_STS10_new", "", chrom_name)
          print(chrom_name)
          if gene in Vitvi04_used_dict.keys():
            print("This gene has be renamed as vitvi04 before in a previous update ! so keep the vitvi04 ID...")
            print(gene)
            print("become ...")
            print(Vitvi04_used_dict[gene])
            new_geneID = Vitvi04_used_dict[gene]
          else:
            if dict_vitvi04_IDs[chrom_name] in Vitvi04_used_dict.values():
              print("This ID vitvi04 is yet used, change !")
              print(dict_vitvi04_IDs[chrom_name])
              while dict_vitvi04_IDs[chrom_name] in Vitvi04_used_dict.values():
                print("While...")
                gene_number=int(re.sub("Vitvi[0-9]{2}[g]0", "", dict_vitvi04_IDs[chrom_name]))
                gene_number+=1
                string= "g0" + str(gene_number)
                dict_vitvi04_IDs[chrom_name]=re.sub("[g][0-9]{5}", string, dict_vitvi04_IDs[chrom_name])
                print(dict_vitvi04_IDs[chrom_name])
            new_geneID = dict_vitvi04_IDs[chrom_name]
            gene_number=int(re.sub("Vitvi[0-9]{2}[g]0", "", dict_vitvi04_IDs[chrom_name]))
            gene_number+=1
            string= "g0" + str(gene_number)
            dict_vitvi04_IDs[chrom_name]=re.sub("[g][0-9]{5}", string, dict_vitvi04_IDs[chrom_name])
          if genes_renaming_dict.get(gene):
            pass
          else:
            genes_renaming_dict[gene] = new_geneID
            list_no_RBH[gene] = new_geneID
      else:
        # here I process the genes which are not of type Vitvi and which do not have RBH in PN12X
        # example : VVRGA10, VVRGA2, VVRGA6
        # I give them an ID "04"
        feature_tmp=db_apollo_updated[gene]
        chrom_name=feature_tmp.chrom
        chrom_number=re.sub("chr", "", chrom_name)
        if gene in Vitvi04_used_dict.keys():
          print("This gene has be renamed as vitvi04 before in a previous update ! so keep the vitvi04 ID...")
          print(gene)
          print("become ...")
          print(Vitvi04_used_dict[gene])
          new_geneID = Vitvi04_used_dict[gene]
        else:
          if dict_vitvi04_IDs[chrom_name] in Vitvi04_used_dict.values():
            print("This ID vitvi04 is yet used, change !")
            print(dict_vitvi04_IDs[chrom_name])
            while dict_vitvi04_IDs[chrom_name] in Vitvi04_used_dict.values():
              print("While...")
              gene_number=int(re.sub("Vitvi[0-9]{2}[g]0", "", dict_vitvi04_IDs[chrom_name]))
              gene_number+=1
              string= "g0" + str(gene_number)
              dict_vitvi04_IDs[chrom_name]=re.sub("[g][0-9]{5}", string, dict_vitvi04_IDs[chrom_name])
              print(dict_vitvi04_IDs[chrom_name])
          new_geneID = dict_vitvi04_IDs[chrom_name]
          gene_number=int(re.sub("Vitvi[0-9]{2}[g]0", "", dict_vitvi04_IDs[chrom_name]))
          gene_number+=1
          string= "g0" + str(gene_number)
          dict_vitvi04_IDs[chrom_name]=re.sub("[g][0-9]{5}", string, dict_vitvi04_IDs[chrom_name])
        if genes_renaming_dict.get(gene):
          print(gene, "found yet in genes_renaming_dict, pass")
          pass
        else:
          genes_renaming_dict[gene] = new_geneID
          list_no_RBH[gene] = new_geneID
  print("Here are the genes (", len(list_no_RBH), " total number of genes) with no RBH on PN12Xv2. So they are replaced by a '04' ID 'PN40024.v4 ID' : 'PN12xV2 ID'")
  print(list_no_RBH)
  return genes_renaming_dict, dict_vitvi04_IDs
  print("------------------")

# here we removed all the PN40024.v4.1 genes that are updated in Apollo
# we add all the updated genes
# and we add all the new genes also
def update_REF_gff3_from_Apollo_gff3(foutput, db_ref, db_apollo_formatted, fref, allTheGenes):
  print("--------------------")
  print("Remove genes from PN40024.v4 gff3 that are updated in Apollo.")
  print("If the two versions of ID are the same, I delete the gene and its children thanks to the ID and I add the new Apollo gene.")
  print("If the Apollo gene ID does not have an ID match in PN40024.v4, I intersect this gene with all the PN40024 genes and find the gene it came from, delete that gene and update with the new one version.")
  print("If the Apollo gene ID is not found in PN40024.v4 and it does not intersect with any gene, then it is a new gene and I add it to the final gff3.")
  db_ref = gffutils.FeatureDB(db_ref)
  db_apollo_updated = gffutils.FeatureDB(db_apollo_formatted)
  genes_and_pseudogenes_iterator_apollo=chain(db_apollo_updated.features_of_type('gene'), db_apollo_updated.features_of_type('pseudogene'))
  list_to_remove=[]
  list_new_genes=[]
  list_to_remove_intersect=[]
  for gene in genes_and_pseudogenes_iterator_apollo:
    mRNA_approved_but_gene_no_status="NO"
    for mRNA in db_apollo_updated.children(gene.id, level=1):
      if ('status' in mRNA.attributes and 'status' not in gene.attributes):
        if ("Approved" in mRNA.attributes['status']):
          mRNA_approved_but_gene_no_status="YES"
    print ("------------")
    print ("------------")
    print ("------------")
    print(gene.id, "begin")
    intersection_dataframe=pd.DataFrame()
    intersection_dataframe2=pd.DataFrame()
    gene_ID=gene.id
    if ('status' in gene.attributes):
      if ("Approved" in gene.attributes['status']):
        # for each gene "approved" in db_apollo_updated, we see if this gene_id is in the PN40024.v4.1, if so we remove it from the ref
        # and we put the new version of the gene
        try :
          # test if gene_ID exists in PN40024.v4.1
          feature_in_ref=db_ref[gene_ID]
          # if yes, we remove this gene from PN40024.v4.1
          list_to_remove.append(gene_ID)
          print(gene_ID, "exists in reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3")
          print("So we will remove", gene_ID, "from ref file and add the updated version from Apollo")
        # then if the gene ID is not in the PN40024.v4.1, we see if there is an intersection of this Apollo gene with the PN40024.v4.1
        # if so, we delete the gene with which it intersects and we put the new version
        except FeatureNotFoundError:
          print(gene_ID, "doesn't exists in reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3, so intersection of this Apollo gene with this REF file ...")
          if os.path.isfile('tmp.gff3'):
            os.remove('tmp.gff3')
          with open('tmp.gff3', 'w') as tmpfile:
            tmpfile.write("{}\n".format(str(db_apollo_updated[gene_ID])))
          a = pybedtools.BedTool('tmp.gff3')
          b = pybedtools.BedTool(fref)
          nb_intersection=(a+b).count()
          intersection=b.intersect(a,s=True,wo=True, r=True, f=0.8, nonamecheck=True)
          if os.path.isfile('tmp_intersect.gff3'):
            os.remove('tmp_intersect.gff3')
          with open('tmp_intersect.gff3', 'w') as tmpfile2:
            tmpfile2.write(str(intersection))
          if os.path.getsize('tmp_intersect.gff3') > 0:
            intersection_dataframe=pd.read_table('tmp_intersect.gff3',header=None)
          # finally, if the gene ID is not in PN40024.v4.1 and the gene does not have a coordinates intersection in PN40024.v4.1, then it is a new one and I add it to ref
          if intersection_dataframe.empty:
            if gene_ID not in list_new_genes:
              print(gene_ID, "has no intersection with reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3 -> this is a new gene !")
              list_new_genes.append(gene_ID)
            # sometimes the intersection is empty because of the 80% filter.
            # but often two PN40024.v4.1 genes are fully included in an Apollo gene which is a merge. But each of them does not represent 80% of the apollo gene so it jumps
            # these genes completely included in an Apollo gene must be deleted and will be added to the list_to_remove_intersect list.
            if os.path.isfile('tmp2.gff3'):
              os.remove('tmp2.gff3')
            with open('tmp2.gff3', 'w') as tmpfile4:
              tmpfile4.write("{}\n".format(str(db_apollo_updated[gene_ID])))
            a_2 = pybedtools.BedTool('tmp2.gff3')
            b_2 = pybedtools.BedTool(fref)
            no_stringent_intersection=b_2.intersect(a_2,s=True,wo=True, r=True, nonamecheck=True)
            if os.path.isfile('tmp_intersect1.gff3'):
              os.remove('tmp_intersect1.gff3')
            with open('tmp_intersect1.gff3', 'w') as tmpfile3:
              tmpfile3.write(str(no_stringent_intersection))
            if os.path.getsize('tmp_intersect1.gff3') > 0:
              intersection_dataframe2=pd.read_table('tmp_intersect1.gff3',header=None)
            if intersection_dataframe2.empty:
              pass
            else:
              for i in range(len(intersection_dataframe2)) :
                if (intersection_dataframe2.iloc[i, 2] == "gene") or (intersection_dataframe2.iloc[i, 2] == "pseudogene"):
                  # if the start of PN40024.v4.1 is larger than Apollo gene and end of PN40024.v4.1 smaller than apollo gene, then it is included in it and we delete it
                  if (int(intersection_dataframe2.iloc[i, 3]) >= int(intersection_dataframe2.iloc[i, 12])) and (int(intersection_dataframe2.iloc[i, 4]) <= int(intersection_dataframe2.iloc[i, 13])):
                    gene_ref=str(intersection_dataframe2.iloc[i, 8]).split(";",1)[0].replace("ID=","")
                    print(gene_ref, "is nested in ", gene_ID, ". So we have to add", gene_ref, "to the list_to_remove_intersect list")
                    list_to_remove_intersect.append(gene_ref)
                    if gene_ID not in list_new_genes:
                      print(gene_ID, "has no intersection with reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3 -> this is a new gene !")
                      list_new_genes.append(gene_ID)
                  else:
                    len_gene_apollo=(int(intersection_dataframe2.iloc[i, 13]) - int(intersection_dataframe2.iloc[i, 12]))
                    len_overlap2=int(intersection_dataframe2.iloc[i, 18])
                    percent_gene_apollo_covered2=int((len_overlap2*100)/len_gene_apollo)
                    if percent_gene_apollo_covered2 >= 5:
                      gene_ref=str(intersection_dataframe2.iloc[i, 8]).split(";",1)[0].replace("ID=","")
                      print("Percent coverage of",gene_ID, " (Apollo gene) on ", gene_ref, " (ref gene) is", percent_gene_apollo_covered2, ". So we remove", gene_ref )
                      print(gene_ref, "intersect with", gene_ID, " on the same strand. So we have to add", gene_ref, "to the list_to_remove_intersect list")
                      list_to_remove_intersect.append(gene_ref)
                      if gene_ID not in list_new_genes:
                        print(gene_ID, "has no intersection with reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3 -> this is a new gene !")
                        list_new_genes.append(gene_ID)
          else:
            print(gene_ID, "has intersection with reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3 -> what is his best overlap? ")
            # dataframe columns :
            # 0 chr REF, 1 source REF, 2 feature REF, 3 start REF, 4 end REF, 5 score REF, 6 strand REF, 7 frame REF, 8 attributes REF
            # 9 chr Apollo , 10 source Apollo, 11 feature Apollo, 12 start Apollo, 13 end Apollo, 14 score Apollo, 15 strand Apollo, 16 frame Apollo, 17 attributes Apollo
            # 18 nbr of overlapping bases
            best_overlap_gene_ref=""
            max_overlap=0
            nb_gene=0
            for i in range(len(intersection_dataframe)) :
              if (intersection_dataframe.iloc[i, 2] == "gene") or (intersection_dataframe.iloc[i, 2] == "pseudogene"):
                nb_gene+=1
            for i in range(len(intersection_dataframe)) :
              if (intersection_dataframe.iloc[i, 2] == "gene") or (intersection_dataframe.iloc[i, 2] == "pseudogene"):
                len_gene_ref=(int(intersection_dataframe.iloc[i, 4]) - int(intersection_dataframe.iloc[i, 3]))
                len_overlap=int(intersection_dataframe.iloc[i, 18])
                percent_gene_ref_covered=int((len_overlap*100)/len_gene_ref)
                if percent_gene_ref_covered > max_overlap:
                  max_overlap=percent_gene_ref_covered
                  best_overlap_gene_ref=str(intersection_dataframe.iloc[i, 8]).split(";",1)[0].replace("ID=","")
                  print("Best coordinates overlap gene for ", gene_ID, "on ref is:", best_overlap_gene_ref)
                  print("So we will remove", best_overlap_gene_ref, "and keep the updated version from Apollo :",gene_ID)
                  list_to_remove_intersect.append(best_overlap_gene_ref)
                  list_new_genes.append(gene_ID)
              elif (nb_gene == 0):
                print(gene_ID, "has no gene pseudogene intersection:")
                print(intersection_dataframe)
                len_gene_ref=(int(intersection_dataframe.iloc[i, 4]) - int(intersection_dataframe.iloc[i, 3]))
                len_overlap=int(intersection_dataframe.iloc[i, 18])
                percent_gene_ref_covered=int((len_overlap*100)/len_gene_ref)
                if percent_gene_ref_covered > max_overlap:
                  max_overlap=percent_gene_ref_covered
                  best_overlap_gene_ref=str(intersection_dataframe.iloc[i, 8]).split(";",1)[0].replace("ID=","")
                  best_overlap_gene_ref=re.sub(".exon.*", "", best_overlap_gene_ref)
                  best_overlap_gene_ref=re.sub("_t0.*", "", best_overlap_gene_ref)
                  print("Best coordinates overlap gene for ", gene_ID, "on ref is:", best_overlap_gene_ref)
                  print("So we will remove", best_overlap_gene_ref, "and keep the updated version from Apollo :",gene_ID)
                  list_to_remove_intersect.append(best_overlap_gene_ref)
                  list_new_genes.append(gene_ID)
    elif (mRNA_approved_but_gene_no_status=="YES"):
      # for each gene "approved" in db_apollo_updated, we see if this gene_id is in the PN40024.v4.1, if so we remove it from the ref
      # and we put the new version of the gene
      try :
        # test if gene_ID exists in PN40024.v4.1
        feature_in_ref=db_ref[gene_ID]
        # if yes, we remove this gene from PN40024.v4.1
        list_to_remove.append(gene_ID)
        print(gene_ID, "exists in reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3")
        print("So we will remove", gene_ID, "from ref file and add the updated version from Apollo")
      # then if the gene ID is not in the PN40024.v4.1, we see if there is an intersection of this Apollo gene with the PN40024.v4.1
      # if so, we delete the gene with which it intersects and we put the new version
      except FeatureNotFoundError:
        print(gene_ID, "doesn't exists in reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3, so intersection of this Apollo gene with this REF file ...")
        if os.path.isfile('tmp.gff3'):
          os.remove('tmp.gff3')
        with open('tmp.gff3', 'w') as tmpfile:
          tmpfile.write("{}\n".format(str(db_apollo_updated[gene_ID])))
        a = pybedtools.BedTool('tmp.gff3')
        b = pybedtools.BedTool(fref)
        nb_intersection=(a+b).count()
        intersection=b.intersect(a,s=True,wo=True, r=True, f=0.8, nonamecheck=True)
        if os.path.isfile('tmp_intersect.gff3'):
          os.remove('tmp_intersect.gff3')
        with open('tmp_intersect.gff3', 'w') as tmpfile2:
          tmpfile2.write(str(intersection))
        if os.path.getsize('tmp_intersect.gff3') > 0:
          intersection_dataframe=pd.read_table('tmp_intersect.gff3',header=None)
        # finally, if the gene ID is not in PN40024.v4.1 and the gene does not have a coordinates intersection in PN40024.v4.1, then it is a new one and I add it to ref
        if intersection_dataframe.empty:
          if gene_ID not in list_new_genes:
            print(gene_ID, "has no intersection with reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3 -> this is a new gene !")
            list_new_genes.append(gene_ID)
          # sometimes the intersection is empty because of the 80% filter.
          # but often two PN40024.v4.1 genes are fully included in an Apollo gene which is a merge. But each of them does not represent 80% of the apollo gene so it jumps
          # these genes completely included in an Apollo gene must be deleted and will be added to the list_to_remove_intersect list.
          if os.path.isfile('tmp2.gff3'):
            os.remove('tmp2.gff3')
          with open('tmp2.gff3', 'w') as tmpfile4:
            tmpfile4.write("{}\n".format(str(db_apollo_updated[gene_ID])))
          a_2 = pybedtools.BedTool('tmp2.gff3')
          b_2 = pybedtools.BedTool(fref)
          no_stringent_intersection=b_2.intersect(a_2,s=True,wo=True, r=True, nonamecheck=True)
          if os.path.isfile('tmp_intersect1.gff3'):
            os.remove('tmp_intersect1.gff3')
          with open('tmp_intersect1.gff3', 'w') as tmpfile3:
            tmpfile3.write(str(no_stringent_intersection))
          if os.path.getsize('tmp_intersect1.gff3') > 0:
            intersection_dataframe2=pd.read_table('tmp_intersect1.gff3',header=None)
          if intersection_dataframe2.empty:
            pass
          else:
            for i in range(len(intersection_dataframe2)) :
              if (intersection_dataframe2.iloc[i, 2] == "gene") or (intersection_dataframe2.iloc[i, 2] == "pseudogene"):
                # if the start of PN40024.v4.1 is larger than Apollo gene and end of PN40024.v4.1 smaller than apollo gene, then it is included in it and we delete it
                if (int(intersection_dataframe2.iloc[i, 3]) >= int(intersection_dataframe2.iloc[i, 12])) and (int(intersection_dataframe2.iloc[i, 4]) <= int(intersection_dataframe2.iloc[i, 13])):
                  gene_ref=str(intersection_dataframe2.iloc[i, 8]).split(";",1)[0].replace("ID=","")
                  print(gene_ref, "is nested in ", gene_ID, ". So we have to add", gene_ref, "to the list_to_remove_intersect list")
                  list_to_remove_intersect.append(gene_ref)
                  if gene_ID not in list_new_genes:
                    print(gene_ID, "has no intersection with reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3 -> this is a new gene !")
                    list_new_genes.append(gene_ID)
                else:
                  len_gene_apollo=(int(intersection_dataframe2.iloc[i, 13]) - int(intersection_dataframe2.iloc[i, 12]))
                  len_overlap2=int(intersection_dataframe2.iloc[i, 18])
                  percent_gene_apollo_covered2=int((len_overlap2*100)/len_gene_apollo)
                  if percent_gene_apollo_covered2 >= 5:
                    gene_ref=str(intersection_dataframe2.iloc[i, 8]).split(";",1)[0].replace("ID=","")
                    print("Percent coverage of",gene_ID, " (Apollo gene) on ", gene_ref, " (ref gene) is", percent_gene_apollo_covered2, ". So we remove", gene_ref )
                    print(gene_ref, "intersect with", gene_ID, " on the same strand. So we have to add", gene_ref, "to the list_to_remove_intersect list")
                    list_to_remove_intersect.append(gene_ref)
                    if gene_ID not in list_new_genes:
                      print(gene_ID, "has no intersection with reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3 -> this is a new gene !")
                      list_new_genes.append(gene_ID)
        else:
          print(gene_ID, "has intersection with reference_files/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3 -> what is his best overlap? ")
          # dataframe columns :
          # 0 chr REF, 1 source REF, 2 feature REF, 3 start REF, 4 end REF, 5 score REF, 6 strand REF, 7 frame REF, 8 attributes REF
          # 9 chr Apollo , 10 source Apollo, 11 feature Apollo, 12 start Apollo, 13 end Apollo, 14 score Apollo, 15 strand Apollo, 16 frame Apollo, 17 attributes Apollo
          # 18 nbr of overlapping bases
          best_overlap_gene_ref=""
          max_overlap=0
          nb_gene=0
          for i in range(len(intersection_dataframe)) :
            if (intersection_dataframe.iloc[i, 2] == "gene") or (intersection_dataframe.iloc[i, 2] == "pseudogene"):
              nb_gene+=1
          for i in range(len(intersection_dataframe)) :
            if (intersection_dataframe.iloc[i, 2] == "gene") or (intersection_dataframe.iloc[i, 2] == "pseudogene"):
              len_gene_ref=(int(intersection_dataframe.iloc[i, 4]) - int(intersection_dataframe.iloc[i, 3]))
              len_overlap=int(intersection_dataframe.iloc[i, 18])
              percent_gene_ref_covered=int((len_overlap*100)/len_gene_ref)
              if percent_gene_ref_covered > max_overlap:
                max_overlap=percent_gene_ref_covered
                best_overlap_gene_ref=str(intersection_dataframe.iloc[i, 8]).split(";",1)[0].replace("ID=","")
                print("Best coordinates overlap gene for ", gene_ID, "on ref is:", best_overlap_gene_ref)
                print("So we will remove", best_overlap_gene_ref, "and keep the updated version from Apollo :",gene_ID)
                list_to_remove_intersect.append(best_overlap_gene_ref)
                list_new_genes.append(gene_ID)
            elif (nb_gene == 0):
              print(gene_ID, "has no gene pseudogene intersection:")
              print(intersection_dataframe)
              len_gene_ref=(int(intersection_dataframe.iloc[i, 4]) - int(intersection_dataframe.iloc[i, 3]))
              len_overlap=int(intersection_dataframe.iloc[i, 18])
              percent_gene_ref_covered=int((len_overlap*100)/len_gene_ref)
              if percent_gene_ref_covered > max_overlap:
                max_overlap=percent_gene_ref_covered
                best_overlap_gene_ref=str(intersection_dataframe.iloc[i, 8]).split(";",1)[0].replace("ID=","")
                best_overlap_gene_ref=re.sub(".exon.*", "", best_overlap_gene_ref)
                best_overlap_gene_ref=re.sub("_t0.*", "", best_overlap_gene_ref)
                print("Best coordinates overlap gene for ", gene_ID, "on ref is:", best_overlap_gene_ref)
                print("So we will remove", best_overlap_gene_ref, "and keep the updated version from Apollo :",gene_ID)
                list_to_remove_intersect.append(best_overlap_gene_ref)
                list_new_genes.append(gene_ID)
  os.remove('tmp_intersect.gff3')
  os.remove('tmp_intersect1.gff3')
  os.remove('tmp.gff3')
  os.remove('tmp2.gff3')
  liste_remove_all=list(set(list_to_remove_intersect + list_to_remove))
  list_new_genesok=list(set(list_new_genes))
  list_new_genes=list_new_genesok
  print("Number of genes with same ID in PN40024.v4 and Apollo version :", len(list_to_remove))
  print(list_to_remove)
  print("Number of PN40024 REF genes with a coordinates intersection on PN40024.v4 (genes to remove from gff3 file):", len(list_to_remove_intersect))
  print(list_to_remove_intersect)
  print("Number of new Apollo genes with no correspondance in PN40024.v4.1:", len(list_new_genes))
  print(list_new_genes)
  with open(foutput) as oldfile, open('clean.gff3', 'w') as newfile:
    for line in oldfile:
      if not any(gene_ID in line for gene_ID in liste_remove_all):
        newfile.write(line)
    for gene in list_to_remove:
      newfile.write("{}\n".format(str(db_apollo_updated[gene])))
      for mRNA in db_apollo_updated.children(gene, level=1):
        newfile.write("{}\n".format(str(mRNA)))
        for exon_cds in sorted(db_apollo_updated.children(mRNA.id, level=1),key=lambda y: (y[3], reversor(y[2]), y[4])):
          newfile.write("{}\n".format(str(exon_cds)))
    for gene in list_new_genes:
      newfile.write("{}\n".format(str(db_apollo_updated[gene])))
      for mRNA in db_apollo_updated.children(gene, level=1):
        newfile.write("{}\n".format(str(mRNA)))
        for exon_cds in sorted(db_apollo_updated.children(mRNA.id, level=1),key=lambda y: (y[3], reversor(y[2]), y[4])):
          newfile.write("{}\n".format(str(exon_cds)))

  print("------")
  print("Here are all the genes removed from PN40024.v4 REF", len(liste_remove_all))
  print(liste_remove_all)
  add_list_total=list_new_genes + list_to_remove
  print("And here all the genes add from Apollo updated version (with updated genes and new genes) :", len(add_list_total))
  print(add_list_total)
  print("At the beginning, we detected", len(allTheGenes), "approved genes.")
  genes_approved_but_not_in_list = np.setdiff1d(add_list_total,allTheGenes).tolist()
  print("Here are the genes approved but not updated or added as new genes in PN40024.v4 :")
  print(genes_approved_but_not_in_list)
  # at the end, we return the new gff3 REF, by copying the clean.gff3 in foutput
  shutil.copyfile('clean.gff3', foutput)
  os.remove('clean.gff3')
  print("--------------------")

# remove all the "to deleted" genes from REF
def delete_TO_DELETE_status_gene_annotations(list_to_delete, foutput):
  print("--------------------")
  print('Deletion of the "to delete" status genes ...')
  to_delete_list=[]
  with open(list_to_delete) as file_in:
    for line in file_in:
      item=line.strip()
      to_delete_list.append(item[ 0 : 13 ])

  print("Number of genes to delete", len(to_delete_list))
  print(to_delete_list)

  with open(foutput) as oldfile, open('clean.gff3', 'w') as newfile:
    for line in oldfile:
      if not any(gene_ID in line for gene_ID in to_delete_list):
        newfile.write(line)

  shutil.copyfile('clean.gff3', foutput)
  os.remove('clean.gff3')
  print("--------------------")

# rename all the gene with their PN12Xv2 ID or "g04...." ID stored in genes_renaming_dict
def renaming_genes_from_dict(genes_renaming_dict, foutput):
  print("--------------------")
  print("Updating gene IDs with their new IDs found with RBH or given with '04' ID")
  print("Total of updated gene IDs : ", len(genes_renaming_dict))
  gffutils.create_db(foutput, "tmp.db", sort_attribute_values=True,merge_strategy="create_unique", force=True)
  tmp_db = gffutils.FeatureDB("tmp.db")
  genes_and_pseudogenes_iterator_allrefgene=chain(tmp_db.features_of_type('gene'), tmp_db.features_of_type('pseudogene'))

  # create a list with all the geneIDs because sometimes the RBH of a new Apollo gene is a PN12v2 ID that is yet in the PN40024.v4.1 file
  # so if I rename the Apollo gene with this RBH, at the end with have duplicated IDs
  # so before renaming an Apollo gene I check if the ID exists and if yes, I put a ".2"
  allrefgene = []
  for gene in genes_and_pseudogenes_iterator_allrefgene:
    allrefgene.append(gene.attributes['ID'][0])

  output_file = open("tmp_final.gff3", 'w')

  genes_and_pseudogenes_iterator_tmp=chain(tmp_db.features_of_type('gene'), tmp_db.features_of_type('pseudogene'))

  for gene in genes_and_pseudogenes_iterator_tmp:
    if (gene.attributes['ID'][0] in genes_renaming_dict) and (genes_renaming_dict[gene.attributes['ID'][0]] not in allrefgene):
      print(gene.attributes['ID'][0], "renamed as", genes_renaming_dict[gene.attributes['ID'][0]])
      old_gene_id=gene.attributes['ID'][0]
      gene.attributes['alias_before_renaming'] = gene.attributes['ID'][0]
      gene.attributes['Name'] = genes_renaming_dict[old_gene_id]
      gene.attributes['ID'] = genes_renaming_dict[old_gene_id]
      output_file.write("{}\n".format(str(gene)))
      for mRNA in tmp_db.children(old_gene_id, level=1):
        old_mRNA_id=mRNA.attributes['ID'][0]
        mRNA.attributes['alias_before_renaming']=mRNA.attributes['ID'][0]
        mRNA.attributes['Name']=mRNA.attributes['Name'][0].replace(old_gene_id, genes_renaming_dict[old_gene_id])
        mRNA.attributes['Parent']=genes_renaming_dict[old_gene_id]
        mRNA.attributes['ID']=mRNA.attributes['ID'][0].replace(old_gene_id, genes_renaming_dict[old_gene_id])
        output_file.write("{}\n".format(str(mRNA)))
        for exon_cds in sorted(tmp_db.children(old_mRNA_id, level=1),key=lambda y: (y[3], reversor(y[2]), y[4])):
          exon_cds.attributes['alias_before_renaming']=exon_cds.attributes['ID'][0]
          exon_cds.attributes['ID']=exon_cds.attributes['ID'][0].replace(old_gene_id, genes_renaming_dict[old_gene_id])
          exon_cds.attributes['Name']=exon_cds.attributes['ID'][0].replace(old_gene_id, genes_renaming_dict[old_gene_id])
          exon_cds.attributes['Parent']=mRNA.attributes['ID'][0].replace(old_gene_id, genes_renaming_dict[old_gene_id])
          output_file.write("{}\n".format(str(exon_cds)))
    elif (gene.attributes['ID'][0] in genes_renaming_dict) and (genes_renaming_dict[gene.attributes['ID'][0]] in allrefgene):
      newGeneID=genes_renaming_dict[gene.attributes['ID'][0]] + ".2"
      print(gene.attributes['ID'][0], "should be renamed as", genes_renaming_dict[gene.attributes['ID'][0]], "but this ID exists yet in PN40024.v4 gff3")
      print("So", gene.attributes['ID'][0], "is renamed as", newGeneID)
      old_gene_id=gene.attributes['ID'][0]
      gene.attributes['alias_before_renaming'] = gene.attributes['ID'][0]
      gene.attributes['Name'] = newGeneID
      gene.attributes['ID'] = newGeneID
      output_file.write("{}\n".format(str(gene)))
      for mRNA in tmp_db.children(old_gene_id, level=1):
        old_mRNA_id=mRNA.attributes['ID'][0]
        mRNA.attributes['alias_before_renaming']=mRNA.attributes['ID'][0]
        mRNA.attributes['Name']=mRNA.attributes['Name'][0].replace(old_gene_id, newGeneID)
        mRNA.attributes['Parent']=newGeneID
        mRNA.attributes['ID']=mRNA.attributes['ID'][0].replace(old_gene_id, newGeneID)
        output_file.write("{}\n".format(str(mRNA)))
        for exon_cds in sorted(tmp_db.children(old_mRNA_id, level=1),key=lambda y: (y[3], reversor(y[2]), y[4])):
          exon_cds.attributes['alias_before_renaming']=exon_cds.attributes['ID'][0]
          exon_cds.attributes['ID']=exon_cds.attributes['ID'][0].replace(old_gene_id, newGeneID)
          exon_cds.attributes['Name']=exon_cds.attributes['ID'][0].replace(old_gene_id, newGeneID)
          exon_cds.attributes['Parent']=mRNA.attributes['ID'][0].replace(old_gene_id, newGeneID)
          output_file.write("{}\n".format(str(exon_cds)))
    else:
      output_file.write("{}\n".format(str(gene)))
      for mRNA in tmp_db.children(gene.id, level=1):
        output_file.write("{}\n".format(str(mRNA)))
        for exon_cds in sorted(tmp_db.children(mRNA.attributes['ID'][0], level=1),key=lambda y: (y[3], reversor(y[2]), y[4])):
          output_file.write("{}\n".format(str(exon_cds)))
  shutil.copyfile("tmp_final.gff3", foutput)
  os.remove("tmp_final.gff3")
  print("--------------------")

if __name__ == '__main__':
  fref, fgff3, foutput, workdir, fasta, oldgff3, reffasta, refcds = command_line()
  # creating the changelog file ...
  changelog = open('%s/changelog' % workdir, 'w')
  sys.stdout = changelog
  today = date.today()
  # dd/mm/YY
  d = today.strftime("%d/%m/%Y")
  now = datetime.now()
  time = now.strftime("%H:%M:%S")
  print("Date of update :", d)
  print("Hour of beginning of update :", time)
  # 1st step : create the databases from the Apollo's gff3 and PN40024.v4 ref
  create_database(oldgff3, "databases/db_genes_oldgff3")
  create_database(fgff3, "databases/db_genes_apollo")
  create_database(fref, "databases/db_genes_ref")
  # in PN40024.v4.1 gff3 file, the Name of transcripts is the gene ID, replace Name by the ID of the transcript
  # and keep Name as oldName attribute
  change_Name_to_ID_PN40024_ref("databases/db_genes_ref",fref)
  fref=str(fref)+ '.Name_formatted.gff3'
  # copy of the PN40024.v4.1 reference file that will be used for updating
  shutil.copyfile(fref, foutput)
  # 2nd step : format the Name and the ID for all the genes, mRNAs and exons/cds
  foutput_temp="Annotations_Apollo.formatted.gff3"
  allTheGenes=renaming_ID_genes_and_children("databases/db_genes_apollo", foutput_temp)
  # 3rd step : create the new Apollo database
  create_database(foutput_temp, "databases/db_genes_apollo_formatted")
  # 4rth step : Find the set of genes to blast
  ffasta="TMP/genes_to_blast.fasta"
  ref_fasta=reffasta
  fasta_cds_REF=refcds
  gene_sequences_to_blast(foutput_temp,ref_fasta,"databases/db_genes_ref","databases/db_genes_apollo_formatted",ffasta,fasta_cds_REF,fref)
  # 5th step : make a reciprocal best hit analysis of the Apollo + PN40024.v4.1 proteins against PN12XV2 proteins
  blastoutput="TMP/reciprocal_best_hits.txt"
  make_blast_RBH(ffasta, blastoutput, fasta)
  ## 6th step : here we create a renaming dictionary of the genes to be renamed:
  ## i.e. genes with an RBH in PN12Xv2 on the same chromosome are renamed with the ID in PN12X
  ## and genes without RBH which are not of type "04" must become of type "04"
  genes_renaming_dict, last_ID_dict=rename_genes(foutput_temp,"databases/db_genes_ref","databases/db_genes_apollo_formatted",blastoutput,"databases/db_genes_oldgff3")
  print("Genes to rename dict ,", len(genes_renaming_dict), "genes :")
  print(genes_renaming_dict)
  print("Here is the dictionnary of last ID for the curators, to give a name to new genes :")
  print(last_ID_dict)
  # 7 step : we delete the PN40024.v4.1 genes which have the same ID as the Apollo genes or the same coordinates and we add the updated genes + the new Apollo genes not present in PN40024.v4.1
  update_REF_gff3_from_Apollo_gff3(foutput, "databases/db_genes_ref", "databases/db_genes_apollo_formatted", fref, allTheGenes)
  # 8 step : we delete from the updated PN40024.v4.2 file with the new annotations, all the apollo annotations with a status "to delete"
  delete_TO_DELETE_status_gene_annotations("to_delete.txt",foutput)
  # 9 : we rename the gene thanks to the dictionnary created in step 6 : genes_renaming_dict
  renaming_genes_from_dict(genes_renaming_dict, foutput)
  # 10 : we generate the UTRs annotations in the final gff3, because Apollo don't create them
  foutput_without_UTRs=foutput + str(".withoutUTRs.gff3")
  command_add_header='''sed "s/_t001_t002/_t002/g" ''' + str(foutput) + '''| awk '$3 == "three_prime_UTR" { next } { print }' | awk '$3 == "five_prime_UTR" { next } { print }' | sed '1i ##gff-version 3' > ''' + foutput_without_UTRs
  os.system(command_add_header)
  print(command_add_header)
  foutput_with_UTRs=foutput + str(".withUTRs.gff3")
  add_utr_command="python add_utrs_to_gff.py {0} > {1}".format(foutput_without_UTRs, foutput_with_UTRs)
  print("Adding UTRs to gff3 file ...")
  print(add_utr_command)
  os.system(add_utr_command)
  # 11 : give a good format to the gff3 and validate it
  foutput_sort = foutput_with_UTRs + str(".sort.FINAL_TO_ADD_APOLLO.gff3")
  # sometime there is a problem with a wrong phase in CDS and gt failed. So I correct this phase if gt fails
  cds_phase_correction_command="sh correction_cds_phase.sh {0} {1} {2}".format(foutput_with_UTRs, foutput_sort, "tmp.log")
  print("CDS phase correction ...")
  print(cds_phase_correction_command)
  os.system(cds_phase_correction_command)
  shutil.copyfile(foutput_sort, foutput)
  os.remove(foutput_sort)
  os.remove(foutput_without_UTRs)
  os.remove(foutput_with_UTRs)
  os.remove("tmp.log")
  time = now.strftime("%H:%M:%S")
  print("Hour of end of update :", time)
  print(foutput, "is updated, you can add it to Apollo !")
  changelog.close()
  os.remove("tmp.db")
  os.remove(ffasta)
  os.remove("to_delete.txt")
  os.remove("TMP/genes_to_blast_final.txt")

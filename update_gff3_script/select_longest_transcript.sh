#!/bin/bash

ffasta=$1
longest_transcript_file=$2

echo "Samtools faidx on ${ffasta}..."
samtools faidx $ffasta
faidx_file="$ffasta.fai"
echo "Retrieve longest transcript ID for each gene ..."
sed 's/_t0.\+//g' $faidx_file | sort -u | parallel -j 10 -k "grep {} -m 1 <(sort -k2,2 -r --sort g $faidx_file) | cut -f1" > $longest_transcript_file
sort -u $longest_transcript_file > $longest_transcript_file.ok
mv $longest_transcript_file.ok $longest_transcript_file
echo "Extract longest transcript sequence for each gene ..."
samtools faidx $ffasta -r $longest_transcript_file > tmp_longest.fasta
mv tmp_longest.fasta $ffasta
rm $faidx_file

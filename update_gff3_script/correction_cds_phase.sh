#!/bin/bash

foutput_with_UTRs=$1
foutput_sort=$2
tmp=$3

if grep "gff-version" $foutput_with_UTRs
then
continue
else
sed -i "1i##gff-version 3" $foutput_with_UTRs
fi

gt gff3 -sort -retainids -checkids $foutput_with_UTRs > $foutput_sort &> $tmp

if grep "gt gff3: error:" $tmp
then
echo "There is an error with gt gff3 command in correction_cds_phase.sh"; exit 1;
else
RET=$?
until [ ${RET} -eq 0 ]; do
  nb_line=$( tail -n1 $tmp | sed "s/.*on line //" | sed "s/ in.*//" )
  wrong_phase=$( tail -n1 $tmp | sed "s/.*wrong phase //" | sed "s/ (.*//")
  probable_phase=$( tail -n1 $tmp | sed "s/.*should be //" | sed "s/)//")
  sed -i "${nb_line}s/\t${wrong_phase}\t/\t${probable_phase}\t/" $foutput_with_UTRs
  gt gff3 -sort -retainids -checkids $foutput_with_UTRs > $foutput_sort &> $tmp
  RET=$?
done
gt gff3 -sort -retainids -checkids $foutput_with_UTRs > $foutput_sort
fi

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/MarieLahaye/PN40024-visualization-tools/">
    <img src="http://www.integrape.eu/templates/mydesign/images/header.png" alt="Logo" width="1200" height="350">
  </a>
  <h3 align="center">Update of PN40024.v4 GFF3 file from Apollo curation work</h3>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## Steps of the script

- 1. Create the gffutils databases from the Apollo gff3 and PN40024.v4.1 REF gff3

create_database() function

- 2. Renaming genes/mRNAs/exons/cds in the gff3 retrieved from Apollo

renaming_ID_genes_and_children() function

- 3. Create the new Apollo database, with renamed genes/mRNAs/exons/cds

create_database() function

- 4. All the gene IDs in Apollo GFF3 not found in the original PN40024.v4.1 gff3 and all the "04" ID in PN40024.v4.1 are retrieved for reciprocal best hit (RBH) against PN12xV2 proteins, in order to rename the PN40024.v4 genes with their RBH in PN12XV2.

gene_sequences_to_blast() function

- 5. This step performs a RBH of all PN40024.v4.1 + Apollo proteins against PN12Xv2

make_blast_RBH() function

- 6. Here, we create a dictionary with all the new gene IDs. Genes with an RBH in PN12Xv2 are renamed with this ID. Genes without an RBH in PN12Xv2 and which don't have a "04" ID are renamed with the last "04" ID of their chromosome.

rename_genes() function

- 7. Here, we remove the genes from PN40024.v4.1 that have been updated and approved in Apollo. Then, we update the PN40024.v4.1 REF gff3 with the Apollo updated/new genes

update_REF_gff3_from_Apollo_gff3() function

- 8. Delete the genes with a "to delete" status from GFF3 

delete_TO_DELETE_status_gene_annotations() function

- 9. Here, all the genes are renamed from the dictionary created in step 6.

renaming_genes_from_dict() function

- 10. Apollo don't give the UTRs in the GFF3, so we determine the UTRs
See add_utr_command variable

- 11. The final updated gff3 file is validated with the genometools sort command.

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* Python 3.8
  ```sh
  wget https://repo.anaconda.com/miniconda/Miniconda3-py38_4.10.3-Linux-x86_64.sh
  chmod 755 Miniconda3-py38_4.10.3-Linux-x86_64.sh
  ./Miniconda3-py38_4.10.3-Linux-x86_64.sh
  # choose a PATH to install miniconda, eg : /work/avelt/PN40024-visualization-tools/update_gff3_script/conda_env
  export PATH=/work/avelt/PN40024-visualization-tools/update_gff3_script/conda_env/bin/:$PATH
  ```
* Packages to install : 
  ```sh
  pip install argparse gffutils datetime numpy Bio pandas pybedtools bcbio-gff
  ```
* Testing packages installation :
```sh
python
```
And then, try to import the following packages :
```sh
import argparse
import warnings
import gffutils
import os
from datetime import date
from datetime import datetime
import sys
import numpy as np
import re
import shutil
from BCBio import GFF
from Bio import SeqIO
from gffutils.exceptions import FeatureNotFoundError
import pandas as pd
warnings.simplefilter("ignore", UserWarning)
import pybedtools
```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/MarieLahaye/PN40024-visualization-tools/
   cd PN40024-visualization-tools/update_gff3_script/
   python update_gff3_apollo.py -h
   ```

- There is blast tool to install. Install with conda :

   ```sh
   conda update --all
   conda install -c bioconda blast
   conda install -c bioconda gffread
   conda install samtools
   conda install -c bioconda parallel
   conda install -c bioconda samtools=1.9 --force-reinstall
   wget http://genometools.org/pub/genometools-1.5.10.tar.gz
   tar -xvf genometools-1.5.10.tar.gz
   cd genometools-1.5.10
   export DESTDIR="/work/avelt/PN40024-visualization-tools/update_gff3_script/genometools-1.5.10"
   make
   export PERL5LIB="/home/avelt/data2/PN40024-visualization-tools/update_gff3_script/conda_env/lib/5.26.2/"
   export PATH=/work/avelt/PN40024-visualization-tools/update_gff3_script/conda_env/bin/:/work/avelt/PN40024-visualization-tools/update_gff3_script/genometools-1.5.10/bin/:$PATH
   ```

Then you can launch update_gff3_apollo.py, all the tools are in your path.

<!-- USAGE EXAMPLES -->
## Usage

python update_gff3_apollo.py -f reference_sequences/vitviv2.pep.fasta -r reference_sequences/PN40024_40X_REF_AnnoV2_V4nomenclature_Final.marie2.gff3 -g Annotations_19_01_2022.gff3 -o Annotations.19_january_2022.gff3 -w /home/avelt/data2/update_gff3_apollo -u PN40024.v4_REF.v2.september_21_2021.gff3


<!-- ROADMAP - TO DO/TO IMPROVE -->
## Roadmap

- For the moment, only the REF chromosomes are processed, not the ALT -> **TO DO**

- For duplicated IDs, I put a ".2", ".3" etc ... on the IDs to make them unique. But it's not perfect -> **TO DO**

Duplicated IDs come from two possible ways : 

 - A gene has been splitted in two new genes, but these two new genes have the same ID in the Apollo gff3 file : example for Vitvi18g03067. 

 - An Apollo gene has an RBH on PN12Xv2, so I want to rename this gene with its RBH, but this ID is yet in PN40024.v4.1 gff3 file -> one case : Vitvi07g02462 should be renamed as Vitvi07g02971 but this ID exists yet in PN40024.v4 gff3

## For guidelines

When we merge two genes in one, I try to remove the two old ones :

<img src="images/img1.png" alt="Logo" width="500" height="150">

-> solution was to remove REF gene when Apollo gene is covered at >= 5% by this gene.
Sometime, an Apollo gene has a smaller size than the PN40024.v4 gene. Example of VIT_08s0007g04580 : 

<img src="images/img3.png" alt="Logo" width="1000" height="250">

So here, the gene ID is not the same between the two genes, so I don't remove Vitvi08g02288. Then, the overlap is of poor quality between the two because less than 80% of genes size overlap, so I don't remove Vitvi08g02288. And then, Vitvi08g02288 is not nested in VIT_08s0007g04580, so I don't remove it. At the end, I keep Vitvi08g02288 and add VIT_08s0007g04580 as a new gene. What is a good solution for this particular case ? Same problem with Vitvi17g01498, Vitvi09g00478, VIT_13s0019g02880 ...
-> solution was to remove REF gene when Apollo gene is covered covered at >= 5% by this gene.

Don't give a duplicated gene ID to new genes, example of duplicated genes after curation on Apollo : Vitvi14g01449, Vitvi16g01453, Vitvi07g02254, Vitvi18g03067 and Vitvi18g02922.

<img src="images/img4.png" alt="Logo" width="500" height="150">

Generally, splitting a gene generates two genes with same ID. My script don't take into account the "a" of the second version : after formatting, the two genes have same IDs, and I put a ".2" for one of them. What is the good solution for this particular case ? 
Eg gene Vitvi14g01449 (chr14:25103295..25124826 (21.53 Kb)), Vitvi18g03067 (Vitvi18g03067.t01 and Vitvi18g03067.t01a), Vitvi18g02922 (Vitvi18g02922_t001 and Vitvi18g02922.t01), Vitvi07g02254 (Vitvi07g02254_t001 and Vitvi07g02254.t01) and Vitvi16g01453.

<img src="images/img5.png" alt="Logo" width="1000" height="250">

Maybe these genes have not been processed by my script, I don't know why for now : 
'K00201_40_H7YJJBBXX_5_2125_21095_15363', 'Vitvi07g02254.2', 'Vitvi16g01453.2', 'Vitvi18g02922.2', 'Vitvi18g03067.2'

<!-- CONTACT -->
## Contact

Amandine Velt - amandine.velt@inrae.fr

Project Link: [https://gitlab.com/MarieLahaye/PN40024-visualization-tools/-/tree/master/update_gff3_script](https://gitlab.com/MarieLahaye/PN40024-visualization-tools/-/tree/master/update_gff3_script)

